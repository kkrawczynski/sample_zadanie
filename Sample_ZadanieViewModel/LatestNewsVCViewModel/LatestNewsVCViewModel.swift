import Foundation
import Sample_ZadanieModel
import RxSwift
import Moya
import RxMoya
import XMLMapper
import ReactiveMoya
import RxRelay

public class LatestNewsVCViewModel {
    // MARK: - Vars
    private let disposeBag = DisposeBag()
    private var techTestProvider: MoyaProvider<TechTestEndpoint>
    private var resourceProvider: MoyaProvider<ResourceEndpoint>
    private var loadLatestNews: BehaviorRelay<()> = BehaviorRelay(value: ())
    private let backgroundScheduler = ConcurrentDispatchQueueScheduler(qos: .background)
    private var latestNewsVM: BehaviorRelay<[LatestNewsViewModel]> = BehaviorRelay(value: [])
    private let activityIndicator = ActivityIndicator()

    public var isWorkingObservable: Observable<Bool> = Observable.just(false)
    public var latestNewsObservable: Observable<[LatestNewsViewModel]> = Observable.just([])

    // MARK: - Inits
    public required init(techProvider: MoyaProvider<TechTestEndpoint>, resProvider: MoyaProvider<ResourceEndpoint>) {

        self.techTestProvider = techProvider
        self.resourceProvider = resProvider

        setupRx()
    }

    // MARK: - Helpers
    private func setupRx() {
        let latestNewsWebRequestObs = self.techTestProvider.rx.request(.latestNews).subscribeOn(backgroundScheduler)
            .filterSuccessfulStatusAndRedirectCodes()
            .asObservable()
            .map { response -> [LatestNewsViewModel] in
                var mappedViewModels: [LatestNewsViewModel] = []
                if let encodedString = String.init(data: response.data, encoding: .utf8) {
                    let rootElement = XMLMapper<LatestNewsRootElement>().map(XMLString: encodedString)

                    rootElement?.channelElements?.forEach { [weak self] channel in
                        guard let self = self else { return }
                            channel.items?.forEach { modelItem in
                                let provider = self.resourceProvider
                                let latestVM = LatestNewsViewModel(model: modelItem, resourceProvider: provider)
                                mappedViewModels.append(latestVM)
                        }
                    }
                }
                return mappedViewModels
        }
            // for indicator test
            .delay(DispatchTimeInterval.seconds(1), scheduler: backgroundScheduler)

        self.loadLatestNews.asObservable().flatMapLatest { [weak self] _ -> Observable<[LatestNewsViewModel]> in
            guard let strongSelf = self else { return Observable.just([]) }
            return latestNewsWebRequestObs.trackActivity(strongSelf.activityIndicator)
        }
        .share(replay: 1)
        .subscribe(onNext: { [weak self] response in
            self?.latestNewsVM.accept(response)
        }).disposed(by: disposeBag)

        self.latestNewsObservable = latestNewsVM.asObservable()

        self.isWorkingObservable = self.activityIndicator.asObservable()
    }
}

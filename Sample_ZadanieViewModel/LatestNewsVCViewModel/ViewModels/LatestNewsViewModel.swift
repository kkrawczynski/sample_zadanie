import Foundation
import RxSwift
import Sample_ZadanieModel
import Moya
import RxMoya

public class LatestNewsViewModel {

    // MARK: - Vars
    private let backgroundScheduler = ConcurrentDispatchQueueScheduler(qos: .background)
    private let activityIndicator = ActivityIndicator()

    public var title: String
    public var newsLinkUrl: URL?
    public var newsDate: String = Constants.blankDate
    public var thumbnailImage: Observable<UIImage?> = Observable.just(nil)
    public var isWorkingObservable = Observable<Bool>.just(false)

    // MARK: - Inits
    public required init(model: LatestNewsItemModel, resourceProvider: MoyaProvider<ResourceEndpoint>) {
        title = model.title ?? Constants.emptyTitle

        if let modelLink = model.link {
            newsLinkUrl = URL(string: modelLink)
        }

        isWorkingObservable = activityIndicator.asObservable()

        if let modelLink = model.thumbnailUrl {

            let randomAsyncNumber = Double.random(in: 0..<2)

            thumbnailImage = resourceProvider.rx.request(.image(url: modelLink))
                .subscribeOn(backgroundScheduler)
                .filterSuccessfulStatusAndRedirectCodes()
                .asObservable().map { response -> UIImage? in
                    let image = UIImage(data: response.data)
                    return image
                }
                // for indicator test
                .delay(DispatchTimeInterval.seconds(Int(randomAsyncNumber)), scheduler: backgroundScheduler)
                .trackActivity(activityIndicator)
        }

        if let modelDate = model.pubDate {
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "EEEE, dd MMMM yyyy HH:mm"
            newsDate = dateFormatter.string(from: modelDate)
        }
    }
}

extension LatestNewsViewModel {
    struct Constants {
        static let emptyTitle = "-"
        static let blankDate = "N\\A"
    }
}

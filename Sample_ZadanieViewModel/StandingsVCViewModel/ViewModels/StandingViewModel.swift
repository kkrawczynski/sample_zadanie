import Foundation
import Sample_ZadanieModel

public class StandingViewModel {
    // MARK: - Vars
    public var clubName: String
    public var rank: Int
    public var totalMatches: Int
    public var matchesWon: Int
    public var matchesDraw: Int
    public var matchesLost: Int
    public var goalsPro: Int
    public var goalsAgainst: Int
    public var points: Int

    public var goalsDiff: Int {
        return goalsPro - goalsAgainst
    }

    // MARK: - Inits
    public required init(withModel standingModel: StandingCompetitionResultModel) {
        clubName = standingModel.clubName ?? Constants.emptyClubName
        rank = standingModel.rank ?? Constants.emptyStanding
        totalMatches = standingModel.matchesTotal ?? Constants.emptyStanding
        matchesWon = standingModel.matchesWon ?? Constants.emptyStanding
        matchesDraw = standingModel.matchesDraw ?? Constants.emptyStanding
        matchesLost = standingModel.matchesLost ?? Constants.emptyStanding
        goalsPro = standingModel.goalsPro ?? Constants.emptyStanding
        goalsAgainst = standingModel.goalsAgainst ?? Constants.emptyStanding
        points = standingModel.points ?? Constants.emptyStanding
    }

}

extension StandingViewModel {
    struct Constants {
        static let emptyClubName = "-"
        static let emptyStanding = 0
    }
}

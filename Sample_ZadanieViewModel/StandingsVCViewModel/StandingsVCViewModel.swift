import Foundation
import Sample_ZadanieModel
import XMLMapper
import Moya
import RxMoya
import RxSwift
import RxRelay

public class StandingsVCViewModel {
    // MARK: - Vars
    private let disposeBag = DisposeBag()
    private let backgroundScheduler = ConcurrentDispatchQueueScheduler(qos: .background)
    private var techTestProvider: MoyaProvider<TechTestEndpoint>
    private var loadStandings = BehaviorRelay(value: ())
    private var standingsVM: BehaviorRelay<[StandingViewModel]> = BehaviorRelay(value: [])
    private let activityIndicator = ActivityIndicator()

    public var standingsVMObservable: Observable<[StandingViewModel]> = Observable.just([])
    public var isWorkingObservable: Observable<Bool> = Observable.just(false)

    // MARK: - Inits
    public required init(provider: MoyaProvider<TechTestEndpoint>) {
        self.techTestProvider = provider
        setupRx()
    }

    // MARK: - Helpers
    private func setupRx() {
        let standingsWebRequestObservable = self.techTestProvider.rx.request(.standings)
            .subscribeOn(backgroundScheduler)
            .filterSuccessfulStatusAndRedirectCodes()
            .asObservable()
            .map { response -> [StandingViewModel] in
                var mappedViewModels: [StandingViewModel] = []
                if let encodedString = String.init(data: response.data, encoding: .utf8) {
                    let rootElement = XMLMapper<StandingRootElement>().map(XMLString: encodedString)
                    rootElement?.competition?.competitionResults?.forEach { result in
                        mappedViewModels.append(StandingViewModel(withModel: result))
                    }
                }
                return mappedViewModels.sorted(by: { (first, second) -> Bool in
                    return first.rank < second.rank
                })
        }

        self.loadStandings.asObservable().flatMapLatest { [weak self] _ -> Observable<[StandingViewModel]> in
            if self != nil {
                return standingsWebRequestObservable.trackActivity(self!.activityIndicator)
            }
            return Observable.just([])

            }
        .share(replay: 1)
        .subscribe(onNext: { [weak self] response in
            self?.standingsVM.accept(response)
        }).disposed(by: disposeBag)

        self.standingsVMObservable = self.standingsVM.asObservable()
        self.isWorkingObservable = self.activityIndicator.asObservable()
    }
}

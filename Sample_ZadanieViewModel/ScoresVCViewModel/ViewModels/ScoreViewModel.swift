import Foundation
import Sample_ZadanieModel

public class ScoreViewModel {
    // MARK: - Vars
    public var teamA: String
    public var teamB: String
    public var scoreDisplay: String {
        let scoreADisplay = teamAScore != nil ? "\(teamAScore!)" : Constants.blankTeamScore
        let scoreBDisplay = teamBScore != nil ? "\(teamBScore!)" : Constants.blankTeamScore
        return "\(scoreADisplay) - \(scoreBDisplay)"
    }

    var teamAScore: Int?
    var teamBScore: Int?

    // MARK: - Inits
    public required init(withScoreModel scoreModel: ScoresCompetitionResultModel) {
        teamA = scoreModel.teamAName ?? Constants.emptyTeamName
        teamB = scoreModel.teamBName ?? Constants.emptyTeamName
        teamAScore = scoreModel.ftScoreA
        teamBScore = scoreModel.ftScoreB
    }
}

extension ScoreViewModel {
    struct Constants {
        static let emptyTeamName = "-"
        static let blankTeamScore = "N\\A"
    }
}

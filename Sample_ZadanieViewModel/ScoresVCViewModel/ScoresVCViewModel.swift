import Foundation
import RxSwift
import Sample_ZadanieModel
import XMLMapper
import RxMoya
import Moya
import RxRelay

public class ScoresVCViewModel {
    // MARK: - Vars
    private let disposeBag = DisposeBag()
    private var provider: MoyaProvider<TechTestEndpoint>!
    private var scoresDate: BehaviorRelay<Date?> = BehaviorRelay(value: nil)
    private var scoresVM: BehaviorRelay<[ScoreViewModel]> = BehaviorRelay(value: [])
    private let backgroundScheduler = ConcurrentDispatchQueueScheduler(qos: .background)
    private var loadScores = BehaviorRelay(value: ())
    private let activityIndicator = ActivityIndicator()

    public var isWorkingObservable: Observable<Bool> = Observable.just(false)
    public var scoresVMObservable: Observable<[ScoreViewModel]> = Observable.just([])
    public var scoresDateObservable: Observable<String> = Observable.just(Constants.emptyDate)

    // MARK: - Inits
    public required init(provider: MoyaProvider<TechTestEndpoint>) {
        self.provider = provider
        setupRx()
    }

    // MARK: - Helpers
    private func setupRx() {
        let scoresWebRequest: Observable<MappedResponse> = self.provider.rx.request(.scores)
            .subscribeOn(backgroundScheduler)
            .filterSuccessfulStatusAndRedirectCodes()
            .asObservable()
            .map { response -> MappedResponse in
                if let encodedString = String.init(data: response.data, encoding: .utf8) {
                    let rootElement = XMLMapper<ScoresRootElement>().map(XMLString: encodedString)

                    var mappedScoresDate: Date?
                    var mappedViewModels: [ScoreViewModel] = []

                    rootElement?.competition?.competitionGroups?.forEach { group in
                        group.competitionResults.map { competitionResults in
                            let mappedResultsViewModels = competitionResults.map {ScoreViewModel(withScoreModel: $0)}
                            mappedViewModels.append(contentsOf: mappedResultsViewModels)
                        }
                    }

                    let scoresDateValue = rootElement?.methodInfo?.parameters?.filter {$0.name == "date"}.first?.value
                    if let dateValue = scoresDateValue {
                        let dateFormatter = DateFormatter()
                        dateFormatter.dateFormat = "yyyy-MM-dd"
                        mappedScoresDate = dateFormatter.date(from: dateValue)
                    }

                    return (mappedScoresDate, mappedViewModels)
                }
                return (nil, [])
        }// for activity indicator test
            .delay(DispatchTimeInterval.seconds(1), scheduler: backgroundScheduler)

        self.loadScores.asObservable().flatMapLatest { [weak self] _  -> Observable<MappedResponse> in
            guard let self = self else { return Observable.just(MappedResponse(scoresDate: nil, scoresVM: [])) }
            return scoresWebRequest.trackActivity(self.activityIndicator)
        }
        .share(replay: 1)
        .subscribe(onNext: { [weak self] mappedResponse in
            self?.scoresDate.accept(mappedResponse.scoresDate)
            self?.scoresVM.accept(mappedResponse.scoresVM)
        }).disposed(by: disposeBag)

        self.scoresDateObservable = self.scoresDate.asObservable().map { value in
            if let dateValue = value {
                let dateFormatter = DateFormatter()
                dateFormatter.dateFormat = "dd MMMM yyyy"
                return dateFormatter.string(from: dateValue)
            }
            return Constants.emptyDate
        }

        self.scoresVMObservable = self.scoresVM.asObservable()
        self.isWorkingObservable = self.activityIndicator.asObservable()
    }

    // MARK: - Public
    public func reloadScores() {
        self.loadScores.accept(())
    }
}

extension ScoresVCViewModel {
    struct Constants {
        static let emptyDate = "-"
    }
}

extension ScoresVCViewModel {
    typealias MappedResponse = (scoresDate: Date?, scoresVM: [ScoreViewModel])
}

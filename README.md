
# Sample Zadanie

Sample app done with requirements specified in Developer_Remote_Programming_Test

## Prerequisites

- Carthage 0.37.0 installed
- Xcode 12.X with Swift 5 support

## Installation

Navigate to folder where Cartfile or Cartfile.resolved file is and execute
```bash
carthage update --platform iOS --no-use-binaries
```
**IF YOU DO NOT WANT TO BUILD CARTHAGE FRAMEWORKS PLEASE DOWNLOAD [ZIPPED FRAMEWORKS](https://bitbucket.org/kkrawczynski/sample_zadanie/downloads/CarthageFrameworks.zip)
THEY ARE BUILT USING SWIFT 5**

## Usage 
Use dropdown menu to navigate between News, Scores & Standings.

![image info](./Documentation/screen1.png)
![image info](./Documentation/screen2.png)
![image info](./Documentation/screen3.png)

You are able to view full news by tapping cell in News table item
![image info](./Documentation/screen4.png)

## Notes 
Run CMD+U to execute unit tests

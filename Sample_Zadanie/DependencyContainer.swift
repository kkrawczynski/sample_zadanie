import Foundation
import Sample_ZadanieModel
import Sample_ZadanieViewModel
import Moya

// QUICK DEPENDENCY CONTAINER TO AVOID E.G. DI framework
public class DependencyContainer {

    public enum Dependencies {
        static let techTestEndpoint = MoyaProvider<TechTestEndpoint>()
        static let resourceProvider = MoyaProvider<ResourceEndpoint>()
    }
}

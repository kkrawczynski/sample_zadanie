import UIKit

struct UIConstants {
    enum StoryboardReuseIdentifiers: String {
        case scoresVCTableViewCell = "ScoresVCTableViewCell"
        case latestNewsVCTableViewCell = "LatestNewsVCTableViewCell"
        case standingsVCTableViewCell = "StandingsVCTableViewCell"
    }

    enum StoryboardSegues: String {
        case toStandingsVC
        case toLatestNewsVC
        case toScoresVC
        case latestNewsVCToLinkWebVC
    }

    enum ImageNames: String {
        case menuVCMainMenuButton = "MenuVC-MenuButton"
        case menuVCMainMenuButtonExpanded = "MenuVC-MenuButtonExpanded"
        case emptyImage = "General-EmptyImage"
    }

    enum Colors {
        static let uiTableViewCellEvenColor = UIColor.white
        static let uiTableViewCellOddColor = UIColor(red: 217/255, green: 217/255, blue: 217/255, alpha: 1)
    }
}

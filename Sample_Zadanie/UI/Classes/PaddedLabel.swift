import UIKit

class PaddedUILabel: UILabel {
    var inset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)

    @IBInspectable public var leftPadding: CGFloat = 0 {
        didSet {
            inset.left = leftPadding
        }
    }

    func setInset(top: CGFloat=0, left: CGFloat=0, right: CGFloat=0, bottom: CGFloat=0) {

        inset.top = top
        inset.left = left
        inset.right = right
        inset.bottom = bottom
    }

    override func drawText(in rect: CGRect) {
        super.drawText(in: rect.inset(by: inset))
    }
}

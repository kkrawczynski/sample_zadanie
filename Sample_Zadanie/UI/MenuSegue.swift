import UIKit

class MenuSegue: UIStoryboardSegue {
    override func perform() {
        if let menuVC = self.source as? MenuViewController {
            if menuVC.displayedViewController != nil {
                remove(viewController: menuVC.displayedViewController!)
            }

            let destinationVC = self.destination
            destinationVC.view.frame = menuVC.childVCContentView.bounds
            destinationVC.view.autoresizingMask = [.flexibleWidth, .flexibleHeight]
            menuVC.addChild(destinationVC)
            menuVC.childVCContentView.addSubview(destinationVC.view)
            menuVC.displayedViewController = destinationVC

        }
    }

    fileprivate func remove(viewController: UIViewController) {
        viewController.view.removeFromSuperview()
        viewController.removeFromParent()
    }
}

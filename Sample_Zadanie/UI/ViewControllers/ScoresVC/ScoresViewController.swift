import UIKit
import RxSwift
import RxCocoa
import Sample_ZadanieViewModel
import RxDataSources

class ScoresViewController: UIViewController {

    // MARK: - Outlets
    @IBOutlet weak var scoresTableView: UITableView!
    @IBOutlet weak var refreshDateLabel: UILabel!

    // MARK: - Vars
    private var scoresVCViewModel = ScoresVCViewModel(provider: DependencyContainer.Dependencies.techTestEndpoint)
    private var backgroundScheduler = ConcurrentDispatchQueueScheduler(qos: .background)
    private let disposeBag = DisposeBag()

    // MARK: - Helpers
    private func setupRx() {

        Observable<Int>.interval(DispatchTimeInterval.seconds(30), scheduler: backgroundScheduler)
            .subscribe(onNext: { [weak self] _ in
                self?.scoresVCViewModel.reloadScores()
            }).disposed(by: disposeBag)

        self.scoresVCViewModel.scoresDateObservable.asDriver(onErrorJustReturn: "-")
            .drive(refreshDateLabel.rx.text).disposed(by: disposeBag)

        self.scoresVCViewModel.isWorkingObservable
            .asDriver(onErrorJustReturn: false)
            .drive(onNext: { [weak self] isWorking in
                if isWorking {
                    self?.view.makeToastActivity(.center)
                } else {
                    self?.view.hideToastActivity()
                }
            }).disposed(by: disposeBag)

        // swiftlint:disable line_length
        let scoresDataSource = RxTableViewSectionedReloadDataSource<SectionModel<String, ScoreViewModel>>(configureCell: {  (_, tableView, idxPath, item) -> UITableViewCell in

            let scoresVCTableCellIdentifier = UIConstants.StoryboardReuseIdentifiers.scoresVCTableViewCell
            let cell = tableView.dequeueReusableCell(withIdentifier: scoresVCTableCellIdentifier.rawValue, for: idxPath)

            guard let scoreCell = cell as? ScoresTableViewCell else {
                return cell
            }
            scoreCell.scoreViewModel = item
            scoreCell.set(style: idxPath.row % 2 == 0 ? .even : .odd)
            return scoreCell
        })

        let scoresDataObservable = scoresVCViewModel.scoresVMObservable
            .map { news in
                return [SectionModel(model: "AllScores", items: news)]
        }

        scoresDataObservable
            .map {section in
                return section.first?.items.count == 0
            }
            .observeOn(MainScheduler.instance)
            .bind(to: scoresTableView.rx.isHidden).disposed(by: disposeBag)

        scoresDataObservable.asDriver(onErrorJustReturn: [])
            .drive(scoresTableView.rx.items(dataSource: scoresDataSource)).disposed(by: disposeBag)

    }

    private func setupUI() {
        scoresTableView.tableFooterView = UIView(frame: .zero)
    }

    // MARK: -  UIViewController
    override func viewDidLoad() {
        super.viewDidLoad()
        setupRx()
        setupUI()
        // Do any additional setup after loading the view.
    }
}

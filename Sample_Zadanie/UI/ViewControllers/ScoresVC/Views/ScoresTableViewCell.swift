import UIKit
import Sample_ZadanieViewModel

class ScoresTableViewCell: UITableViewCell {

    // MARK: - Outlets
    @IBOutlet weak var teamBLabel: PaddedUILabel!
    @IBOutlet weak var scoreLabel: UILabel!
    @IBOutlet weak var teamALabel: PaddedUILabel!

    // MARK: - Vars
    public var scoreViewModel: ScoreViewModel? {
        didSet {
            guard let viewModel = scoreViewModel else {
                return
            }
            teamALabel.text = viewModel.teamA
            teamBLabel.text = viewModel.teamB
            scoreLabel.text = viewModel.scoreDisplay
        }
    }

    // MARK: - Helpers
    public func set(style: UITableViewCell.Style) {
        switch style {
        case .even:
            self.backgroundColor = UIConstants.Colors.uiTableViewCellEvenColor
        default:
            self.backgroundColor = UIConstants.Colors.uiTableViewCellOddColor
        }
    }

    // MARK:  UITableViewCell
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        teamBLabel.setInset(left: 10, right: 10)
        teamALabel.setInset(left: 10, right: 10)
    }
}

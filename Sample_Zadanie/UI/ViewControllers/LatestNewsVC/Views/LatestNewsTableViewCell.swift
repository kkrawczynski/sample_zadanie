import UIKit
import Sample_ZadanieViewModel
import RxSwift
import RxCocoa

class LatestNewsTableViewCell: UITableViewCell {

    // MARK: - Outlets
    @IBOutlet weak var newsImageView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!

    // MARK: - Vars
    private let disposeBag = DisposeBag()

    public var viewModel: LatestNewsViewModel? {
        didSet {
            guard let viewModel = viewModel else {
                return
            }

            titleLabel.text = viewModel.title
            dateLabel.text = viewModel.newsDate

            viewModel.thumbnailImage
                .asDriver(onErrorJustReturn: UIImage(named: UIConstants.ImageNames.emptyImage.rawValue))
                .do(onNext: { [weak self] _ in
                    UIView.animate(withDuration: 0.3, animations: {
                        self?.newsImageView?.alpha = 1
                    })
                })
                .drive(newsImageView.rx.image)
                .disposed(by: disposeBag)

            viewModel.isWorkingObservable
                .map {!$0}.asDriver(onErrorJustReturn: false)
                .do(onNext: { [weak self] isHidden in
                    if !isHidden {
                        self?.activityIndicator.startAnimating()
                    }
                })
                .drive(activityIndicator.rx.isHidden).disposed(by: disposeBag)
        }
    }

    // MARK: -  UITableViewCell
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    override func layoutSubviews() {
        super.layoutSubviews()
        contentView.frame = contentView.frame.inset(by: UIEdgeInsets(top: 10, left: 10, bottom: 0, right: 10))
        contentView.layer.cornerRadius = 10
    }
}

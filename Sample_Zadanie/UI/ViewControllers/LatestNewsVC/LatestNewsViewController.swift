import UIKit
import RxSwift
import RxDataSources
import Sample_ZadanieViewModel

class LatestNewsViewController: UIViewController {

    // MARK: - Outlets
    @IBOutlet weak var newsTableView: UITableView!

    // MARK: - Vars
    // swiftlint:disable line_length
    private var latestNewsVCViewModel = LatestNewsVCViewModel(techProvider: DependencyContainer.Dependencies.techTestEndpoint, resProvider: DependencyContainer.Dependencies.resourceProvider)
    private var backgroundScheduler = ConcurrentDispatchQueueScheduler(qos: .background)
    private let disposeBag = DisposeBag()

    // MARK: - Helpers
    private func setupRx() {

        latestNewsVCViewModel.isWorkingObservable
            .asDriver(onErrorJustReturn: false)
            .drive(onNext: { [weak self] isWorking in
                if isWorking {
                    self?.view.makeToastActivity(.center)
                } else {
                    self?.view.hideToastActivity()
                }
            }).disposed(by: disposeBag)

        let newsDataSource = RxTableViewSectionedReloadDataSource<SectionModel<String, LatestNewsViewModel>>(configureCell: {  (_, tableView, idxPath, item) -> UITableViewCell in

            let cell = tableView.dequeueReusableCell(withIdentifier: UIConstants.StoryboardReuseIdentifiers.latestNewsVCTableViewCell.rawValue, for: idxPath)

            guard let newsCell = cell as? LatestNewsTableViewCell else {
                return cell
            }
            newsCell.viewModel = item
            return newsCell
        })

        let latestNewsDataObservable = latestNewsVCViewModel.latestNewsObservable
            .map { news in
                return [SectionModel(model: "AllNews", items: news)]
        }

        latestNewsDataObservable
            .map {section in
                return section.first?.items.count == 0
            }
            .observeOn(MainScheduler.instance)
            .bind(to: newsTableView.rx.isHidden).disposed(by: disposeBag)

        newsTableView.rx.modelSelected(LatestNewsViewModel.self)
            .subscribe(onNext: { [unowned self] selectedViewModel in
                self.performSegue(withIdentifier: UIConstants.StoryboardSegues.latestNewsVCToLinkWebVC.rawValue, sender: selectedViewModel.newsLinkUrl)
        }).disposed(by: disposeBag)

        latestNewsDataObservable.asDriver(onErrorJustReturn: []).drive(newsTableView.rx.items(dataSource: newsDataSource)).disposed(by: disposeBag)

    }

    private func setupUI() {
        newsTableView.tableFooterView = UIView(frame: .zero)
    }

    // MARK: -  UIViewController
    override func viewDidLoad() {
        super.viewDidLoad()
        setupRx()
        setupUI()
        // Do any additional setup after loading the view.
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == UIConstants.StoryboardSegues.latestNewsVCToLinkWebVC.rawValue, let destinationVC = segue.destination as? LinkWebViewController {
            if let passedUrl = sender as? URL {
                destinationVC.urlToLoad = passedUrl
            } else {
                self.view.makeToast("No Link Provided", duration: TimeInterval(3))
            }
        }
    }
}

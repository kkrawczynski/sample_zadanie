import UIKit
import WebKit
import RxCocoa
import RxSwift

class LinkWebViewController: UIViewController {

    // MARK: - Outlets
    @IBOutlet weak var doneButton: UIBarButtonItem!
    @IBOutlet weak var webView: WKWebView!

    // MARK: - Vars
    private let disposeBag = DisposeBag()
    public var urlToLoad: URL?

    // MARK: - Helpers
    private func setupRx() {
        doneButton.rx.tap.subscribe(onNext: { [unowned self] _ in
            self.dismissController()
        }).disposed(by: disposeBag)
    }

    private func setupUI() {
        if let passedUrlToLoad = urlToLoad {
            webView.load(URLRequest(url: passedUrlToLoad))
        } else {
            dismissController()
        }
    }

    private func dismissController() {
        self.dismiss(animated: true, completion: nil)
    }

    // MARK: -  UIViewController
    override func viewDidLoad() {
        super.viewDidLoad()
        setupRx()
        setupUI()
        // Do any additional setup after loading the view.
    }
}

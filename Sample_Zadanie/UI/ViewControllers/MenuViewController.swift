import UIKit

class MenuViewController: UIViewController {

    // MARK: - Outlets
    @IBOutlet var menuButtons: [UIButton]!
    @IBOutlet weak var backgroundImageView: UIImageView!
    @IBOutlet weak var mainMenuButton: UIButton!
    @IBOutlet weak var childVCContentView: UIView!

    // MARK: - Vars
    var displayedViewController: UIViewController?

    // MARK: - Helpers
    func setupUI() {
        UIView.animate(withDuration: 0.5, delay: 0, options: .curveEaseIn, animations: {
            self.backgroundImageView.alpha = 0.2
            // self.view.backgroundColor = UIColor.white.withAlphaComponent(0.5)
            self.mainMenuButton.alpha = 1
        })

        self.performSegue(withIdentifier: UIConstants.StoryboardSegues.toLatestNewsVC.rawValue, sender: nil)
    }

    private func animateMenuButtons() {
        let isHidden = menuButtons.first?.isHidden ?? true
        menuButtons.forEach { button in
            UIView.animate(withDuration: 0.3, delay: 0, options: .curveEaseOut, animations: {
                button.isHidden = !button.isHidden
                self.view.layoutIfNeeded()
            })
        }

        UIView.animate(withDuration: 0.3, delay: 0, options: .curveEaseOut, animations: {
            let rotationAngle = isHidden ? (-Double.pi / 2) : 0

            var menuImageString = ""
            switch isHidden {
            case true:
                menuImageString = UIConstants.ImageNames.menuVCMainMenuButtonExpanded.rawValue
            case false:
                menuImageString = UIConstants.ImageNames.menuVCMainMenuButton.rawValue
            }

            self.mainMenuButton.imageView?.transform = CGAffineTransform(rotationAngle: CGFloat(rotationAngle))
            self.mainMenuButton.setImage(UIImage(named: menuImageString), for: .normal)
        })
    }

    // MARK: - UI Events
    @IBAction func menuButtonTapped(_ sender: UIButton) {
        switch sender.tag {
        case MenuButtonsIdentifier.news.rawValue:
            self.performSegue(withIdentifier: UIConstants.StoryboardSegues.toLatestNewsVC.rawValue, sender: nil)
        case MenuButtonsIdentifier.scores.rawValue:
            self.performSegue(withIdentifier: UIConstants.StoryboardSegues.toScoresVC.rawValue, sender: nil)
        case MenuButtonsIdentifier.standings.rawValue:
            self.performSegue(withIdentifier: UIConstants.StoryboardSegues.toStandingsVC.rawValue, sender: nil)
        default:
            break
        }
        animateMenuButtons()
    }

    @IBAction func mainMenuButtonTap(_ sender: UIButton) {
        animateMenuButtons()
    }

    // MARK: -  UIViewController
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
        // Do any additional setup after loading the view.
    }
}

extension MenuViewController {
    enum MenuButtonsIdentifier: Int {
        case news = 1
        case scores = 2
        case standings = 3
    }
}

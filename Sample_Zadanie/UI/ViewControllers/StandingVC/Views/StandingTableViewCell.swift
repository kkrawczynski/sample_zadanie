import UIKit
import Sample_ZadanieViewModel

class StandingTableViewCell: UITableViewCell {

    // MARK: - Outlets
    @IBOutlet weak var rankLabel: UILabel!
    @IBOutlet weak var clubNameLabel: PaddedUILabel!
    @IBOutlet weak var playedLabel: UILabel!
    @IBOutlet weak var winLabel: UILabel!
    @IBOutlet weak var drawLabel: UILabel!
    @IBOutlet weak var lostLabel: UILabel!
    @IBOutlet weak var goalsDiffLabel: UILabel!
    @IBOutlet weak var pointsLabel: UILabel!

    // MARK: - Vars
    public var standingViewModel: StandingViewModel? {
        didSet {
            guard let viewModel = standingViewModel else {
                return
            }
            rankLabel.text = "\(viewModel.rank)"
            clubNameLabel.text = viewModel.clubName
            playedLabel.text = "\(viewModel.totalMatches)"
            winLabel.text = "\(viewModel.matchesWon)"
            drawLabel.text = "\(viewModel.matchesDraw)"
            lostLabel.text = "\(viewModel.matchesLost)"
            goalsDiffLabel.text = "\(viewModel.goalsDiff)"
            pointsLabel.text = "\(viewModel.points)"
        }
    }

    // MARK: - Helpers
    public func set(style: UITableViewCell.Style) {
        switch style {
        case .even:
            self.backgroundColor = UIConstants.Colors.uiTableViewCellEvenColor
        default:
            self.backgroundColor = UIConstants.Colors.uiTableViewCellOddColor
        }
    }

    // MARK: -  UITableViewCell
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
}

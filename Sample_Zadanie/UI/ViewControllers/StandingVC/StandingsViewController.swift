import UIKit
import Sample_ZadanieViewModel
import RxSwift
import RxCocoa
import RxDataSources

class StandingsViewController: UIViewController {

    // MARK: - Outlets
    @IBOutlet weak var standingsTableView: UITableView!
    @IBOutlet weak var titleView: UIView!

    // MARK: - Vars
    private var standingsCViewModel = StandingsVCViewModel(provider: DependencyContainer.Dependencies.techTestEndpoint)
    private var backgroundScheduler = ConcurrentDispatchQueueScheduler(qos: .background)
    private let disposeBag = DisposeBag()

    // MARK: - Helpers
    private func setupRx() {
        standingsCViewModel.isWorkingObservable
            .asDriver(onErrorJustReturn: false)
            .drive(onNext: { [weak self] isWorking in
                if isWorking {
                    self?.view.makeToastActivity(.center)
                } else {
                    self?.view.hideToastActivity()
                }
            }).disposed(by: disposeBag)

        // swiftlint:disable line_length
        let standingsDataSource = RxTableViewSectionedReloadDataSource<SectionModel<String, StandingViewModel>>(configureCell: {  (_, tableView, idxPath, item) -> UITableViewCell in

            let standingsVCCellIdentifier = UIConstants.StoryboardReuseIdentifiers.standingsVCTableViewCell
            let cell = tableView.dequeueReusableCell(withIdentifier: standingsVCCellIdentifier.rawValue, for: idxPath)

            guard let standingCell = cell as? StandingTableViewCell else {
                return cell
            }
            standingCell.standingViewModel = item
            standingCell.set(style: idxPath.row % 2 == 0 ? .even : .odd)
            return standingCell
        })

        let standingsDataObservable = standingsCViewModel.standingsVMObservable
            .map { news in
                return [SectionModel(model: "AllStandings", items: news)]
        }

        let standingWithResultsObservable =  standingsDataObservable
            .map {section in
                return section.first?.items.count == 0
            }
            .observeOn(MainScheduler.instance)

        standingWithResultsObservable.bind(to: standingsTableView.rx.isHidden).disposed(by: disposeBag)
        standingWithResultsObservable.bind(to: titleView.rx.isHidden).disposed(by: disposeBag)

        standingsDataObservable.asDriver(onErrorJustReturn: [])
            .drive(standingsTableView.rx.items(dataSource: standingsDataSource)).disposed(by: disposeBag)
    }

    private func setupUI() {
        standingsTableView.tableFooterView = UIView(frame: .zero)
    }

    // MARK: -  UIViewController
    override func viewDidLoad() {
        super.viewDidLoad()
        setupRx()
        setupUI()
        // Do any additional setup after loading the view.
    }
}

import Quick
import Nimble
import XMLMapper
import Moya
import RxSwift
import RxMoya

@testable import Sample_ZadanieModel

class ProviderSpec: BaseSpec {
    // swiftlint:disable function_body_length
    override func spec() {
        super.spec()

        var disposeBag = DisposeBag()

        beforeEach {
            disposeBag = DisposeBag()
        }

        it("return data") {

            let provider = MoyaProvider<TechTestEndpoint>(stubClosure: MoyaProvider.immediatelyStub)
            var assertedData: StandingRootElement?

            let response = provider.rx.request(.standings)

            response.subscribe { response in

                switch response {
                case .success(let response):
                    if let encodedString = String.init(data: response.data, encoding: .utf8) {
                        assertedData = XMLMapper<StandingRootElement>().map(XMLString: encodedString)
                    }
                case .error : break
                }
                }.disposed(by: disposeBag)

            expect(assertedData).toEventuallyNot(beNil())
            expect(assertedData?.competition?.competitionName).toEventually(equal("Premier League"))
            expect(assertedData?.competition?.competitionResults?.count).toEventually(equal(20))

        }
        it("return internal error") {
            var assertedError: Error?

            let internalError = NSError(domain: "Internal iOS Error", code: -1234, userInfo: nil)

            let customEndpointClosure = { (target: TechTestEndpoint) -> Endpoint in
                return Endpoint(url: URL(target: target).absoluteString,
                                sampleResponseClosure: { .networkError(internalError) },
                                method: target.method,
                                task: target.task,
                                httpHeaderFields: target.headers)
            }

            // swiftlint:disable line_length
            let provider = MoyaProvider<TechTestEndpoint>(endpointClosure: customEndpointClosure, stubClosure: MoyaProvider.immediatelyStub)

            let response = provider.rx.request(.standings)

            response.subscribe { response in
                switch response {
                case .success: break
                case .error(let error):
                    assertedError = error
                }
                }.disposed(by: disposeBag)

            expect(assertedError).notTo(beNil())
        }
        it("return 404") {
            let errorData = "<rss version=\"2.0\">".data(using: .utf8)!

            let customEndpointClosure = { (target: TechTestEndpoint) -> Endpoint in
                return Endpoint(url: URL(target: target).absoluteString,
                                sampleResponseClosure: { .networkResponse(404, errorData) },
                                method: target.method,
                                task: target.task,
                                httpHeaderFields: target.headers)
            }

            // swiftlint:disable line_length
            let provider = MoyaProvider<TechTestEndpoint>(endpointClosure: customEndpointClosure, stubClosure: MoyaProvider.immediatelyStub)

            let response = provider.rx.request(.standings)
            var assertedData: StandingRootElement?

            response.subscribe { response in
                switch response {
                case .success(let response):
                    if let encodedString = String.init(data: response.data, encoding: .utf8) {
                        assertedData = XMLMapper<StandingRootElement>().map(XMLString: encodedString)
                    }
                case .error:break
                }
                }.disposed(by: disposeBag)

            expect(assertedData).to(beNil())
        }
    }
}

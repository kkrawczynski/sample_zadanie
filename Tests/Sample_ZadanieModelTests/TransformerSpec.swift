import Quick
import Nimble
import XMLMapper

@testable import Sample_ZadanieModel

class TransformerSpec: BaseSpec {
    override func spec() {
        super.spec()

        describe("LatestNewsDateTransformer Spec") {
            describe("will parse date from string") {
                it("with UTC") {
                    let stringDate = "Wed, 9 Jan 2013 18:30:46 +0000"
                    let assertedDate = LatestNewsDateTransformer().transformFromXML(stringDate)

                    var calendar = Calendar(identifier: .gregorian)
                    calendar.timeZone = TimeZone(abbreviation: "UTC")!
                    expect(assertedDate).notTo(beNil())
                    expect(calendar.component(.year, from: assertedDate!)).to(equal(2013))
                    expect(calendar.component(.month, from: assertedDate!)).to(equal(1))
                    expect(calendar.component(.day, from: assertedDate!)).to(equal(9))
                    expect(calendar.component(.hour, from: assertedDate!)).to(equal(18))
                    expect(calendar.component(.minute, from: assertedDate!)).to(equal(30))
                    expect(calendar.component(.second, from: assertedDate!)).to(equal(46))
                    expect(calendar.component(.weekday, from: assertedDate!)).to(equal(4))
                }
                it("converting to GMT+2") {
                    let stringDate = "Wed, 9 Jan 2013 18:30:46 +0000"
                    let assertedDate = LatestNewsDateTransformer().transformFromXML(stringDate)

                    var calendar = Calendar(identifier: .gregorian)
                    calendar.timeZone = TimeZone(abbreviation: "GMT+0200")!
                    expect(assertedDate).notTo(beNil())
                    expect(calendar.component(.year, from: assertedDate!)).to(equal(2013))
                    expect(calendar.component(.month, from: assertedDate!)).to(equal(1))
                    expect(calendar.component(.day, from: assertedDate!)).to(equal(9))
                    expect(calendar.component(.hour, from: assertedDate!)).to(equal(20))
                    expect(calendar.component(.minute, from: assertedDate!)).to(equal(30))
                    expect(calendar.component(.second, from: assertedDate!)).to(equal(46))
                    expect(calendar.component(.weekday, from: assertedDate!)).to(equal(4))
                }
            }
        }
    }
}

import Quick
import Nimble
import XMLMapper

@testable import Sample_ZadanieModel

class ScoresMappingSpec: BaseSpec {
    // swiftlint:disable function_body_length
    override func spec() {
        super.spec()

        describe("ScoresCompetitionResult") {
            it("mapping 1") {
                let scoresCompItemXML = Mocks.Mappings.scoresCompetition1
                let assertedModel = XMLMapper<ScoresCompetitionResultModel>().map(XMLString: scoresCompItemXML.rawValue)

                expect(assertedModel).notTo(beNil())
                expect(assertedModel?.ftScoreA).to(equal(0))
                expect(assertedModel?.ftScoreB).to(equal(3))
                expect(assertedModel?.teamACountry).to(equal("ENG"))
                expect(assertedModel?.teamAName).to(equal("Chelsea"))
                expect(assertedModel?.teamBCountry).to(equal("ITA"))
                expect(assertedModel?.teamBName).to(equal("Juventus"))

            }
            it("mapping 2") {
                let scoresCompItemXML = Mocks.Mappings.scoresCompetition2
                let assertedModel = XMLMapper<ScoresCompetitionResultModel>().map(XMLString: scoresCompItemXML.rawValue)

                expect(assertedModel).notTo(beNil())
                expect(assertedModel?.ftScoreA).to(equal(0))
                expect(assertedModel?.ftScoreB).to(equal(1))
                expect(assertedModel?.teamACountry).to(equal("UKR"))
                expect(assertedModel?.teamAName).to(equal("Shakhtar Donetsk"))
                expect(assertedModel?.teamBCountry).to(equal("DNK"))
                expect(assertedModel?.teamBName).to(equal("Nordsjælland"))

            }
        }

        describe("ScoreCompetitionResults") {
            it("mapping with match results") {
                let scoresCompResultsXML = Mocks.Mappings.scoresCompetitionResults
                let assertedModel = XMLMapper<ScoresCompetitionModel>().map(XMLString: scoresCompResultsXML.rawValue)

                expect(assertedModel).notTo(beNil())
                expect(assertedModel?.name).to(equal("UEFA Champions League"))
                expect(assertedModel?.competitionGroups).notTo(beNil())
                expect(assertedModel?.competitionGroups?.count).to(equal(4))

                let assertedFirstGroup = assertedModel?.competitionGroups?.first
                expect(assertedFirstGroup?.groupName).to(equal("Group E"))
                expect(assertedFirstGroup?.competitionResults).notTo(beNil())
                expect(assertedFirstGroup?.competitionResults?.count).to(equal(2))

                let assertedFirstCompetitionResults = assertedFirstGroup?.competitionResults?.first
                expect(assertedFirstCompetitionResults?.ftScoreA).to(equal(0))
                expect(assertedFirstCompetitionResults?.ftScoreB).to(equal(3))
                expect(assertedFirstCompetitionResults?.teamACountry).to(equal("ENG"))
                expect(assertedFirstCompetitionResults?.teamAName).to(equal("Chelsea"))
                expect(assertedFirstCompetitionResults?.teamBName).to(equal("Juventus"))
                expect(assertedFirstCompetitionResults?.teamBCountry).to(equal("ITA"))

            }
        }
        it("Scores Full Response") {
            let scoresResponseXMLString = Mocks.Mappings.fullResponse
            let assertedModel = XMLMapper<ScoresRootElement>().map(XMLString: scoresResponseXMLString.rawValue)

            expect(assertedModel).notTo(beNil())
            expect(assertedModel?.competition).notTo(beNil())
            expect(assertedModel?.methodInfo).notTo(beNil())

            let assertedMethod = assertedModel?.methodInfo
            expect(assertedMethod?.parameters?.count).to(equal(7))
            expect(assertedMethod?.parameters?.first {$0.name == "date"}?.value).to(equal("2012-09-19"))

            let assertedCompetition = assertedModel?.competition
            expect(assertedCompetition?.competitionGroups?.count).to(equal(4))
            expect(assertedCompetition?.competitionGroups?.first?.groupName).to(equal("Group E"))

            let assertedCompetitionGroupResults = assertedCompetition?.competitionGroups?.first?.competitionResults

            expect(assertedCompetitionGroupResults?.count).to(equal(2))

            let assertedGroupFirstResult = assertedCompetitionGroupResults?.first
            expect(assertedGroupFirstResult?.ftScoreA).to(equal(0))
            expect(assertedGroupFirstResult?.ftScoreB).to(equal(3))
            expect(assertedGroupFirstResult?.teamACountry).to(equal("ENG"))
            expect(assertedGroupFirstResult?.teamAName).to(equal("Chelsea"))
            expect(assertedGroupFirstResult?.teamBCountry).to(equal("ITA"))
            expect(assertedGroupFirstResult?.teamBName).to(equal("Juventus"))

        }

        it("Scores Full Response Wrong XML Mapping") {
            let scoresResponseXMLString = Mocks.Mappings.fullResponseWrongSchema
            let assertedModel = XMLMapper<ScoresRootElement>().map(XMLString: scoresResponseXMLString.rawValue)
            expect(assertedModel).notTo(beNil())
            expect(assertedModel?.competition).to(beNil())
            expect(assertedModel?.competition?.competitionGroups).to(beNil())

        }
    }
}

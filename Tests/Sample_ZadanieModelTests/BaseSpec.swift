import Foundation
import Quick
import Nimble

class BaseSpec: QuickSpec {
    override func spec() {
        super.spec()

        Nimble.AsyncDefaults.timeout = DispatchTimeInterval.seconds(10)
    }
}

import Quick
import Nimble
import XMLMapper

@testable import Sample_ZadanieModel

class StandingsMappingSpec: BaseSpec {
    // swiftlint:disable function_body_length
    override func spec() {
        super.spec()

        it("Standings Full Response") {
            let standingsFullResponseXMLString = Mocks.Mappings.fullResponse
            let assertedModel = XMLMapper<StandingRootElement>().map(XMLString: standingsFullResponseXMLString.rawValue)

            expect(assertedModel).notTo(beNil())
            expect(assertedModel?.competition).notTo(beNil())

            let assertedCompetition = assertedModel?.competition
            expect(assertedCompetition?.competitionName).to(equal("Premier League"))
            expect(assertedCompetition?.competitionResults?.count).to(equal(20))

            let assertedCompetitionFirstResult = assertedCompetition?.competitionResults?.first
            expect(assertedCompetitionFirstResult?.clubName).to(equal("Manchester City"))
            expect(assertedCompetitionFirstResult?.goalsAgainst).to(equal(29))
            expect(assertedCompetitionFirstResult?.goalsPro).to(equal(93))
            expect(assertedCompetitionFirstResult?.lastRank).to(equal(1))
            expect(assertedCompetitionFirstResult?.matchesDraw).to(equal(5))
            expect(assertedCompetitionFirstResult?.matchesLost).to(equal(5))
            expect(assertedCompetitionFirstResult?.matchesTotal).to(equal(38))
            expect(assertedCompetitionFirstResult?.matchesWon).to(equal(28))
            expect(assertedCompetitionFirstResult?.points).to(equal(89))
            expect(assertedCompetitionFirstResult?.rank).to(equal(1))
        }

        it("Standing Competition") {
            let standingsCompetitionXMLString = Mocks.Mappings.competition
            let competitionXml = standingsCompetitionXMLString.rawValue
            let assertedModel = XMLMapper<StandingCompetitionModel>().map(XMLString: competitionXml)

            expect(assertedModel).notTo(beNil())
            expect(assertedModel?.competitionResults?.count).to(equal(20))

            let assertedCompetitionLastResult = assertedModel?.competitionResults?.last
            expect(assertedCompetitionLastResult?.clubName).to(equal("Wolverhampton Wanderers"))
            expect(assertedCompetitionLastResult?.goalsAgainst).to(equal(82))
            expect(assertedCompetitionLastResult?.goalsPro).to(equal(40))
            expect(assertedCompetitionLastResult?.lastRank).to(equal(20))
            expect(assertedCompetitionLastResult?.matchesDraw).to(equal(10))
            expect(assertedCompetitionLastResult?.matchesLost).to(equal(23))
            expect(assertedCompetitionLastResult?.matchesTotal).to(equal(38))
            expect(assertedCompetitionLastResult?.matchesWon).to(equal(5))
            expect(assertedCompetitionLastResult?.points).to(equal(25))
            expect(assertedCompetitionLastResult?.rank).to(equal(20))
        }

        it("Standings Full Response Wrong XML Mapping") {
            let standingsFullResponseXMLString = Mocks.Mappings.fullResponseWrongSchema
            let assertedModel = XMLMapper<StandingRootElement>().map(XMLString: standingsFullResponseXMLString.rawValue)

            expect(assertedModel).notTo(beNil())
            expect(assertedModel?.competition).to(beNil())
            expect(assertedModel?.competition?.competitionResults).to(beNil())
        }
    }
}

import Quick
import Nimble
import XMLMapper

@testable import Sample_ZadanieModel

class LatestNewsMappingSpec: BaseSpec {
    // swiftlint:disable function_body_length
    override func spec() {
        super.spec()

        describe("LatestNewsItem Model") {
            it("mapping 1") {
                let latestNewsItemXMLString = Mocks.Mappings.latestItemMapping1
                let assertedModel = XMLMapper<LatestNewsItemModel>().map(XMLString: latestNewsItemXMLString.rawValue)
                expect(assertedModel).notTo(beNil())
                expect(assertedModel?.category).to(equal("News"))
                expect(assertedModel?.description).to(equal("Lead story teaser text here"))
                expect(assertedModel?.title).to(equal("Lead story headline goes here"))
                // swiftlint:disable line_length
                let assrtLnk = "https://www.goal.com/en/news/pedro-leon-how-real-madrids-potential-star-became-mourinhos/1r5mn1imd1hko1gh912x9m8bs7"
                expect(assertedModel?.link).to(equal(assrtLnk))

                var calendar = Calendar(identifier: .gregorian)
                calendar.timeZone = TimeZone(abbreviation: "UTC")!
                let assertedDate = assertedModel?.pubDate ?? Date()

                expect(calendar.component(.year, from: assertedDate)).to(equal(2013))
                expect(calendar.component(.month, from: assertedDate)).to(equal(1))
                expect(calendar.component(.day, from: assertedDate)).to(equal(1))
                expect(calendar.component(.hour, from: assertedDate)).to(equal(17))
                expect(calendar.component(.minute, from: assertedDate)).to(equal(0))
                expect(calendar.component(.second, from: assertedDate)).to(equal(0))
                expect(calendar.component(.weekday, from: assertedDate)).to(equal(3))
                let assrtThumb = "http://www.mobilefeeds.performgroup.com/javaImages/4b/ce/0,,14012~11849291,00.jpg"
                expect(assertedModel?.thumbnailUrl).to(equal(assrtThumb))
            }
            it("mapping 2") {
                let latestNewsItemXMLString = Mocks.Mappings.latestItemMapping2
                let assertedModel = XMLMapper<LatestNewsItemModel>().map(XMLString: latestNewsItemXMLString.rawValue)
                expect(assertedModel).notTo(beNil())
                expect(assertedModel?.category).to(equal("News"))
                expect(assertedModel?.title).to(equal("3rd story headline goes here"))
                expect(assertedModel?.description).to(equal("Description test"))
                // swiftlint:disable line_length
                let assrtLnk = "https://www.goal.com/en/news/pedro-leon-how-real-madrids-potential-star-became-mourinhos/1r5mn1imd1hko1gh912x9m8bs7"
                expect(assertedModel?.link).to(equal(assrtLnk))

                var calendar = Calendar(identifier: .gregorian)
                calendar.timeZone = TimeZone(abbreviation: "UTC")!
                let assertedDate = assertedModel?.pubDate ?? Date()

                expect(calendar.component(.year, from: assertedDate)).to(equal(2013))
                expect(calendar.component(.month, from: assertedDate)).to(equal(1))
                expect(calendar.component(.day, from: assertedDate)).to(equal(1))
                expect(calendar.component(.hour, from: assertedDate)).to(equal(15))
                expect(calendar.component(.minute, from: assertedDate)).to(equal(0))
                expect(calendar.component(.second, from: assertedDate)).to(equal(0))
                expect(calendar.component(.weekday, from: assertedDate)).to(equal(3))
                let assrThumbnail = "http://www.mobilefeeds.performgroup.com/javaImages/4b/ce/0,,14012~11849291,00.jpg"
                expect(assertedModel?.thumbnailUrl).to(equal(assrThumbnail))
            }
        }
        describe("LatestNewsChannel Model") {
            it("mapping without items") {

                let latestNewsXMLString = Mocks.Mappings.latestChannelMappingNoItems
                let assertedModel = XMLMapper<LatestNewsChannelModel>().map(XMLString: latestNewsXMLString.rawValue)
                expect(assertedModel).notTo(beNil())
                expect(assertedModel?.categories?.first).to(equal("interview"))
                expect(assertedModel?.categories?.last).to(equal("Perform"))
                expect(assertedModel?.items).to(beNil())
                expect(assertedModel?.language).to(equal("en-GB"))
                let assertedLink = "https://www.goal.com/en"
                expect(assertedModel?.link).to(equal(assertedLink))
                expect(assertedModel?.pubDate).notTo(beNil())
                expect(assertedModel?.description).to(equal("TEST desc"))
                expect(assertedModel?.title).to(equal("Perform Mobile Test - Latest News"))
            }
        }
        it("Scores Full response") {
            let latestNewsXmlString = Mocks.Mappings.fullResponse

            let assertedRootElement = XMLMapper<LatestNewsRootElement>().map(XMLString: latestNewsXmlString.rawValue)
            expect(assertedRootElement).notTo(beNil())

            let assertedModel = assertedRootElement?.channelElements?.first
            expect(assertedModel).notTo(beNil())
            expect(assertedModel?.categories?.first).to(equal("interview2"))
            expect(assertedModel?.categories?.last).to(equal("Perform2"))
            expect(assertedModel?.items?.count).to(equal(2))

            let firstItem = assertedModel?.items?.first

            expect(firstItem).notTo(beNil())
            expect(firstItem?.category).to(equal("News"))
            expect(firstItem?.description).to(equal("Description test"))
            expect(firstItem?.title).to(equal("3rd story headline goes here"))
            // swiftlint:disable line_length
            let assrtLnk = "https://www.goal.com/en/news/pedro-leon-how-real-madrids-potential-star-became-mourinhos/1r5mn1imd1hko1gh912x9m8bs7"
            expect(firstItem?.link).to(equal(assrtLnk))

            var calendar = Calendar(identifier: .gregorian)
            calendar.timeZone = TimeZone(abbreviation: "UTC")!
            let assertedDate = firstItem?.pubDate ?? Date()

            expect(calendar.component(.year, from: assertedDate)).to(equal(2013))
            expect(calendar.component(.month, from: assertedDate)).to(equal(1))
            expect(calendar.component(.day, from: assertedDate)).to(equal(1))
            expect(calendar.component(.hour, from: assertedDate)).to(equal(15))
            expect(calendar.component(.minute, from: assertedDate)).to(equal(0))
            expect(calendar.component(.second, from: assertedDate)).to(equal(0))
            expect(calendar.component(.weekday, from: assertedDate)).to(equal(3))
            let assertedThumbnail = "http://www.mobilefeeds.performgroup.com/javaImages/4b/ce/0,,14012~11849291,00.jpg"
            expect(firstItem?.thumbnailUrl).to(equal(assertedThumbnail))

            expect(assertedModel?.language).to(equal("en-GB2"))
            expect(assertedModel?.link).to(equal("https://www.goal.com/en2"))
            expect(assertedModel?.pubDate).notTo(beNil())
            expect(assertedModel?.description).to(equal("TEST desc2"))
            expect(assertedModel?.title).to(equal("Perform Mobile Test - Latest News2"))
        }

        it("Scores Full Response Wrong XML Mapping") {
            let latestNewsXmlString = Mocks.Mappings.fullResponseWrongSchema

            let assertedRootElement = XMLMapper<LatestNewsRootElement>().map(XMLString: latestNewsXmlString.rawValue)
            expect(assertedRootElement).to(beNil())
        }
    }
}

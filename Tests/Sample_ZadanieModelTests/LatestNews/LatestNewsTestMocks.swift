// swiftlint:disable all
extension LatestNewsMappingSpec {
    struct Mocks {
        enum Mappings: String {
            case latestItemMapping1 = """
            <item>
            <guid>100001</guid>
            <title>Lead story headline goes here</title>
            <pubDate>Tue, 01 Jan 2013 17:00:00 +0000</pubDate>
            <enclosure length=\"9048\" url=\"http://www.mobilefeeds.performgroup.com/javaImages/4b/ce/0,,14012~11849291,00.jpg\" type=\"image/jpeg\"/>
            <description>Lead story teaser text here</description>
            <link>
            https://www.goal.com/en/news/pedro-leon-how-real-madrids-potential-star-became-mourinhos/1r5mn1imd1hko1gh912x9m8bs7
            </link>
            <category>News</category>
            </item>
            """
            case latestItemMapping2 = """
            <item>
            <guid>100003</guid>
            <title>3rd story headline goes here</title>
            <pubDate>Tue, 01 Jan 2013 15:00:00 +0000</pubDate>
            <enclosure length="9048" url="http://www.mobilefeeds.performgroup.com/javaImages/4b/ce/0,,14012~11849291,00.jpg" type="image/jpeg"/>
            <description>Description test</description>
            <link>
            https://www.goal.com/en/news/pedro-leon-how-real-madrids-potential-star-became-mourinhos/1r5mn1imd1hko1gh912x9m8bs7
            </link>
            <category>News</category>
            </item>
            """
            case latestChannelMappingNoItems = """
            <channel>
            <title>Perform Mobile Test - Latest News</title>
            <description>TEST desc</description>
            <language>en-GB</language>
            <pubDate>Tue, 01 Jan 2013 18:00:00 +0000</pubDate>
            <link>
            https://www.goal.com/en
            </link>
            <category>interview</category>
            <category>Perform</category>
            </channel>
            """
            case fullResponse = """
            <rss version="2.0">
            <channel>
            <title>Perform Mobile Test - Latest News2</title>
            <description>TEST desc2</description>
            <language>en-GB2</language>
            <pubDate>Tue, 01 Jan 2013 18:00:00 +0000</pubDate>
            <link>
            https://www.goal.com/en2
            </link>
            <category>interview2</category>
            <category>Perform2</category>
            <item>
            <guid>100003</guid>
            <title>3rd story headline goes here</title>
            <pubDate>Tue, 01 Jan 2013 15:00:00 +0000</pubDate>
            <enclosure length="9048" url="http://www.mobilefeeds.performgroup.com/javaImages/4b/ce/0,,14012~11849291,00.jpg" type="image/jpeg"/>
            <description>Description test</description>
            <link>
            https://www.goal.com/en/news/pedro-leon-how-real-madrids-potential-star-became-mourinhos/1r5mn1imd1hko1gh912x9m8bs7
            </link>
            <category>News</category>
            </item>
            <item>
            <guid>100003</guid>
            <title>3rd story headline goes here</title>
            <pubDate>Tue, 01 Jan 2013 15:00:00 +0000</pubDate>
            <enclosure length="9048" url="http://www.mobilefeeds.performgroup.com/javaImages/4b/ce/0,,14012~11849291,00.jpg" type="image/jpeg"/>
            <description>Description test</description>
            <link>
            https://www.goal.com/en/news/pedro-leon-how-real-madrids-potential-star-became-mourinhos/1r5mn1imd1hko1gh912x9m8bs7
            </link>
            <category>News</category>
            </item>
            </channel>
            </rss>
            """
            case fullResponseWrongSchema = """
            <rss version="2.0">
            <test_root>
            <channel>
            <title>Perform Mobile Test - Latest News2</title>
            <description>TEST desc2</description>
            <language>en-GB2</language>
            <pubDate>Tue, 01 Jan 2013 18:00:00 +0000</pubDate>
            <link>
            https://www.goal.com/en2
            </link>
            <category>interview2</category>
            <category>Perform2</category>
            </channel>
            </test_root>
            </rss>
            """
        }
    }
}
// swiftlint:enable all

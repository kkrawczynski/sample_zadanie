import Foundation
import Quick
import Nimble
import Sample_ZadanieModel

@testable import Sample_ZadanieViewModel

class StandingViewModelSpec: BaseSpec {
    override func spec() {
        super.spec()

        describe("StandingViewModel") {
            it("with model") {
                // swiftlint:disable line_length
                let mockedXml = """
                <ranking rank="1" last_rank="1" zone_start="CL" team_id="676" club_name="Manchester City" countrycode="ENG" area_id="68" matches_total="38" matches_won="28" matches_draw="5" matches_lost="5" goals_pro="93" goals_against="29" points="89"/>
                """

                let mockedModel = StandingCompetitionResultModel(XMLString: mockedXml)!
                let assertedVM = StandingViewModel(withModel: mockedModel)

                expect(assertedVM).notTo(beNil())
                expect(assertedVM.clubName).to(equal("Manchester City"))
                expect(assertedVM.goalsAgainst).to(equal(29))
                expect(assertedVM.goalsPro).to(equal(93))
                expect(assertedVM.matchesDraw).to(equal(5))
                expect(assertedVM.matchesLost).to(equal(5))
                expect(assertedVM.matchesWon).to(equal(28))
                expect(assertedVM.points).to(equal(89))
                expect(assertedVM.rank).to(equal(1))
                expect(assertedVM.totalMatches).to(equal(38))
            }
            it("without model") {
                let mockedXml = """
                <ranking zone_start="CL" team_id="676" />
                """

                let mockedModel = StandingCompetitionResultModel(XMLString: mockedXml)!
                let assertedVM = StandingViewModel(withModel: mockedModel)

                expect(assertedVM).notTo(beNil())
                expect(assertedVM.clubName).to(equal("-"))
                expect(assertedVM.goalsAgainst).to(equal(0))
                expect(assertedVM.goalsPro).to(equal(0))
                expect(assertedVM.matchesDraw).to(equal(0))
                expect(assertedVM.matchesLost).to(equal(0))
                expect(assertedVM.matchesWon).to(equal(0))
                expect(assertedVM.points).to(equal(0))
                expect(assertedVM.rank).to(equal(0))
                expect(assertedVM.totalMatches).to(equal(0))
            }
        }
    }
}

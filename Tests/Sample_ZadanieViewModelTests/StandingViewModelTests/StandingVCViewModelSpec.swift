import Foundation
import Quick
import Nimble
import RxSwift
import Moya
import Sample_ZadanieModel
@testable import Sample_ZadanieViewModel

class StandingVCViewModelSpec: BaseSpec {
    override func spec() {
        super.spec()

        var disposeBag = DisposeBag()

        describe("StandingVCViewModel") {
            beforeEach {
                disposeBag = DisposeBag()
            }
            describe("will load up") {
                beforeEach {
                    disposeBag = DisposeBag()
                }
                it("StandingViewModels") {
                    let moyaProvider = MoyaProvider<TechTestEndpoint>(stubClosure: MoyaProvider.immediatelyStub)
                    let assertedVM = StandingsVCViewModel(provider: moyaProvider)

                    var assertedValues: [StandingViewModel]?

                    assertedVM.standingsVMObservable.subscribe(onNext: { val in
                        assertedValues = val
                    }).disposed(by: disposeBag)

                    expect(assertedValues?.count).toEventually(equal(20))

                }
                it("and sort ascending") {

                    let unsortedData = Constants.unsortedXMLData.data(using: .utf8)!

                    let customEndpointClosure = { (target: TechTestEndpoint) -> Endpoint in
                        return Endpoint(url: URL(target: target).absoluteString,
                                        sampleResponseClosure: { .networkResponse(200, unsortedData) },
                                        method: target.method,
                                        task: target.task,
                                        httpHeaderFields: target.headers)
                    }

                    // swiftlint:disable line_length
                    let moyaProvider = MoyaProvider<TechTestEndpoint>(endpointClosure: customEndpointClosure, stubClosure: MoyaProvider.immediatelyStub)
                    let assertedVM = StandingsVCViewModel(provider: moyaProvider)

                    var assertedValues: [StandingViewModel]?

                    assertedVM.standingsVMObservable.subscribe(onNext: { val in
                        assertedValues = val
                    }).disposed(by: disposeBag)

                    expect(assertedValues?.count).toEventually(equal(5))
                    expect(assertedValues?.first?.clubName).toEventually(equal("Tottenham Hotspur"))
                    expect(assertedValues?.first?.rank).toEventually(equal(1))
                    expect(assertedValues?.last?.clubName).toEventually(equal("Newcastle United"))
                    expect(assertedValues?.last?.rank).toEventually(equal(5))
                }
            }
        }
    }
}

// swiftlint:disable all
extension StandingVCViewModelSpec {
    struct Constants {
        static let unsortedXMLData = """
<gsmrs version="2.0" sport="soccer" lang="en" last_generated="2013-04-17 15:26:15">
<method method_id="12" name="get_tables">
<parameter name="id" value="5983"/>
<parameter name="lang" value="en"/>
<parameter name="tabletype" value="total"/>
<parameter name="type" value="season"/>
</method>
<competition competition_id="8" name="Premier League" teamtype="default" display_order="10" type="club" area_id="68" area_name="England" last_updated="2013-04-17 15:21:27" soccertype="default">
<season season_id="5983" name="2011/2012" start_date="2011-08-13" end_date="2012-05-30" service_level="0" last_updated="2012-07-12 10:07:57" last_playedmatch_date="2012-05-13">
<round round_id="14829" name="Regular Season" start_date="2011-08-13" end_date="2012-05-30" type="table" ordermethod="1" groups="0" has_outgroup_matches="no">
<resultstable type="total">
<ranking rank="4" last_rank="1" zone_start="CL" team_id="676" club_name="Manchester City" countrycode="ENG" area_id="68" matches_total="38" matches_won="28" matches_draw="5" matches_lost="5" goals_pro="93" goals_against="29" points="89"/>
<ranking rank="2" last_rank="2" team_id="662" club_name="Manchester United" countrycode="ENG" area_id="68" matches_total="38" matches_won="28" matches_draw="5" matches_lost="5" goals_pro="89" goals_against="33" points="89"/>
<ranking rank="3" last_rank="3" zone_end="CL" team_id="660" club_name="Arsenal" countrycode="ENG" area_id="68" matches_total="38" matches_won="21" matches_draw="7" matches_lost="10" goals_pro="74" goals_against="49" points="70"/>
<ranking rank="1" last_rank="4" zone_start="CL qf" zone_end="CL qf" team_id="675" club_name="Tottenham Hotspur" countrycode="ENG" area_id="68" matches_total="38" matches_won="20" matches_draw="9" matches_lost="9" goals_pro="66" goals_against="41" points="69"/>
<ranking rank="5" last_rank="5" zone_start="EuroL" zone_end="EuroL" team_id="664" club_name="Newcastle United" countrycode="ENG" area_id="68" matches_total="38" matches_won="19" matches_draw="8" matches_lost="11" goals_pro="56" goals_against="51" points="65"/>

</resultstable>
</round>
</season>
</competition>
</gsmrs>
"""
    }
}

import Quick
import Nimble
import Sample_ZadanieModel

@testable import Sample_ZadanieViewModel

class ScoreViewModelSpec: BaseSpec {
    override func spec() {
        super.spec()

        it("ScoreViewModelSpec with model") {
            // swiftlint:disable line_length
            let mockedXml = """
            <match match_id="1388726" date_utc="2012-09-19" time_utc="18:45:00" date_london="2012-09-19" time_london="19:45:00" team_A_id="1682" team_A_name="Sporting Braga" team_A_country="PRT" team_B_id="1824" team_B_name="CFR Cluj" team_B_country="ROU" status="Playing" gameweek="1" winner="" fs_A="0" fs_B="3" hts_A="" hts_B="" ets_A="" ets_B="" ps_A="" ps_B="" last_updated="2012-09-19 22:59:51"/>
            """
            let mockedModel = ScoresCompetitionResultModel(XMLString: mockedXml)

            let assertedViewModel = ScoreViewModel(withScoreModel: mockedModel!)
            expect(assertedViewModel.teamA).to(equal("Sporting Braga"))
            expect(assertedViewModel.teamB).to(equal("CFR Cluj"))
            expect(assertedViewModel.teamAScore).to(equal(0))
            expect(assertedViewModel.teamBScore).to(equal(3))
            expect(assertedViewModel.scoreDisplay).to(equal("0 - 3"))

        }
        it("ScoreViewModelSpec with defaults") {
            let mockedXml = """
            <match match_id="1388726" gameweek="1" winner="" />
            """
            let mockedModel = ScoresCompetitionResultModel(XMLString: mockedXml)

            let assertedViewModel = ScoreViewModel(withScoreModel: mockedModel!)
            expect(assertedViewModel.teamA).to(equal("-"))
            expect(assertedViewModel.teamB).to(equal("-"))
            expect(assertedViewModel.teamAScore).to(beNil())
            expect(assertedViewModel.teamBScore).to(beNil())
            expect(assertedViewModel.scoreDisplay).to(equal("N\\A - N\\A"))
        }
    }
}

import Quick
import Nimble
import RxSwift
import RxMoya
import Sample_ZadanieModel
import Moya

@testable import Sample_ZadanieViewModel

class ScoreVCViewModelSpec: BaseSpec {
    // swiftlint:disable function_body_length
    override func spec() {
        super.spec()

        var disposeBag = DisposeBag()

        describe("ScoreVCViewModel") {
            beforeEach {
                disposeBag = DisposeBag()
            }

            describe("will load up") {
                beforeEach {
                    disposeBag = DisposeBag()
                }
                it("ScoreViewModels") {
                    let moyaProvider = MoyaProvider<TechTestEndpoint>(stubClosure: MoyaProvider.immediatelyStub)
                    let assertedVM = ScoresVCViewModel(provider: moyaProvider)

                    var assertedValues: [ScoreViewModel]?

                    assertedVM.scoresVMObservable.subscribe(onNext: { val in
                        assertedValues = val
                    }).disposed(by: disposeBag)

                    assertedVM.reloadScores()

                    expect(assertedValues?.count).toEventually(equal(8))

                }
                it("and format Score date") {
                    let moyaProvider = MoyaProvider<TechTestEndpoint>(stubClosure: MoyaProvider.immediatelyStub)
                    let assertedVM = ScoresVCViewModel(provider: moyaProvider)

                    var assertedDate: String?

                    assertedVM.scoresDateObservable.subscribe(onNext: { val in
                        assertedDate = val
                    }).disposed(by: disposeBag)

                    assertedVM.reloadScores()

                    expect(assertedDate).toEventually(equal("19 September 2012"))
                }
            }
            it("will show defaults on error") {

                let internalError = NSError(domain: "Internal iOS Error", code: -1234, userInfo: nil)

                let customEndpointClosure = { (target: TechTestEndpoint) -> Endpoint in
                    return Endpoint(url: URL(target: target).absoluteString,
                                    sampleResponseClosure: { .networkError(internalError) },
                                    method: target.method,
                                    task: target.task,
                                    httpHeaderFields: target.headers)
                }
                // swiftlint:disable line_length
                let moyaProvider = MoyaProvider<TechTestEndpoint>( endpointClosure: customEndpointClosure, stubClosure: MoyaProvider.immediatelyStub)
                let assertedVM = ScoresVCViewModel(provider: moyaProvider)

                var assertedValues: [ScoreViewModel]?
                var assertedDate: String?

                assertedVM.scoresVMObservable.subscribe(onNext: { val in
                    assertedValues = val
                }).disposed(by: disposeBag)

                assertedVM.scoresDateObservable.subscribe(onNext: { val in
                    assertedDate = val
                }).disposed(by: disposeBag)

                assertedVM.reloadScores()

                expect(assertedValues?.count).toEventually(equal(0))
                expect(assertedDate).toEventually(equal("-"))
            }
        }
    }
}

import Quick
import Nimble
import Sample_ZadanieModel
import Moya
import RxSwift

@testable import Sample_ZadanieViewModel

class LatestNewsViewModelSpec: BaseSpec {
    // swiftlint:disable function_body_length
    override func spec() {
        super.spec()

        var disposeBag = DisposeBag()
        beforeEach {
            disposeBag = DisposeBag()
        }

        it("LatestNewsViewModel with model") {
            // swiftlint:disable line_length
            let mockedXml = """
            <item>
            <guid>100003</guid>
            <title>3rd story headline goes here</title>
            <pubDate>Tue, 01 Jan 2013 15:00:00 +0100</pubDate>
            <enclosure length="9048" url="http://www.mobilefeeds.performgroup.com/javaImages/4b/ce/0,,14012~11849291,00.jpg" type="image/jpeg"/>
            <description>3rd story teaser text here</description>
            <link>
            https://www.goal.com/en/news/pedro-leon-how-real-madrids-potential-star-became-mourinhos/1r5mn1imd1hko1gh912x9m8bs7
            </link>
            <category>News</category>
            </item>
            """
            let mockedModel = LatestNewsItemModel(XMLString: mockedXml)
            let stubProvider = MoyaProvider<ResourceEndpoint>(stubClosure: MoyaProvider.immediatelyStub)
            let assertedViewModel = LatestNewsViewModel(model: mockedModel!, resourceProvider: stubProvider)

            var assertedImage: UIImage?
            assertedViewModel.thumbnailImage.subscribe(onNext: { image in
                assertedImage = image
            }).disposed(by: disposeBag)

            expect(assertedViewModel).notTo(beNil())
            expect(assertedViewModel.newsDate).to(equal("Tuesday, 01 January 2013 15:00"))
            expect(assertedViewModel.title).to(equal("3rd story headline goes here"))
            expect(assertedImage).toEventuallyNot(beNil())
        }

        it("LatestNewsViewModel with defaults") {
            let mockedXml = """
            <item>
            <guid>100003</guid>
            </item>
            """
            let mockedModel = LatestNewsItemModel(XMLString: mockedXml)
            let stubProvider = MoyaProvider<ResourceEndpoint>(stubClosure: MoyaProvider.immediatelyStub)
            let assertedViewModel = LatestNewsViewModel(model: mockedModel!, resourceProvider: stubProvider)

            var assertedImage: UIImage?
            assertedViewModel.thumbnailImage.subscribe(onNext: { image in
                assertedImage = image
            }).disposed(by: disposeBag)

            expect(assertedViewModel).notTo(beNil())
            expect(assertedViewModel.newsDate).to(equal("N\\A"))
            expect(assertedViewModel.title).to(equal("-"))
            expect(assertedImage).toEventually(beNil())
        }

    }
}

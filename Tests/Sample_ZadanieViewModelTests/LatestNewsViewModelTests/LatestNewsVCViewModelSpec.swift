import Foundation
import Quick
import Nimble
import Moya
import RxSwift
import Sample_ZadanieModel

@testable import Sample_ZadanieViewModel

class LatestNewsVCViewModelSpec: BaseSpec {
    // swiftlint:disable function_body_length
    override func spec() {
        super.spec()

        var disposeBag = DisposeBag()

        describe("LatestNewsVCViewModel") {
            beforeEach {
                disposeBag = DisposeBag()
            }
            describe("will load up") {
                beforeEach {
                    disposeBag = DisposeBag()
                }

                it("LatestNewsViewModels") {
                    let moyaProvider = MoyaProvider<TechTestEndpoint>(stubClosure: MoyaProvider.immediatelyStub)
                    let resourceProvider = MoyaProvider<ResourceEndpoint>(stubClosure: MoyaProvider.immediatelyStub)

                    let assertedVM = LatestNewsVCViewModel(techProvider: moyaProvider, resProvider: resourceProvider)

                    var assertedNewsItemVMs: [LatestNewsViewModel]?

                    assertedVM.latestNewsObservable.subscribe(onNext: { value in
                        assertedNewsItemVMs = value
                    }).disposed(by: disposeBag)

                    expect(assertedNewsItemVMs?.count).toEventually(equal(10))
                }
                it("defaults") {
                    let emptyData = "<rss version=\"2.0\">".data(using: .utf8)!

                    let customEndpointClosure = { (target: TechTestEndpoint) -> Endpoint in
                        return Endpoint(url: URL(target: target).absoluteString,
                                        sampleResponseClosure: { .networkResponse(200, emptyData) },
                                        method: target.method,
                                        task: target.task,
                                        httpHeaderFields: target.headers)
                    }

                    // swiftlint:disable line_length
                    let moyaProvider = MoyaProvider<TechTestEndpoint>(endpointClosure: customEndpointClosure, stubClosure: MoyaProvider.immediatelyStub)
                    let resourceProvider = MoyaProvider<ResourceEndpoint>(stubClosure: MoyaProvider.immediatelyStub)

                    var assertedNewsItemVMs: [LatestNewsViewModel]?
                    let assertedVM = LatestNewsVCViewModel(techProvider: moyaProvider, resProvider: resourceProvider)
                    assertedVM.latestNewsObservable.subscribe(onNext: { value in
                        assertedNewsItemVMs = value
                    }).disposed(by: disposeBag)

                    expect(assertedNewsItemVMs).toEventuallyNot(beNil())
                    expect(assertedNewsItemVMs?.count).toEventually(equal(0))
                }
            }
            it("will show defaults on error") {
                let internalError = NSError(domain: "Internal iOS Error", code: -1234, userInfo: nil)

                let customEndpointClosure = { (target: TechTestEndpoint) -> Endpoint in
                    return Endpoint(url: URL(target: target).absoluteString,
                                    sampleResponseClosure: { .networkError(internalError) },
                                    method: target.method,
                                    task: target.task,
                                    httpHeaderFields: target.headers)
                }

                // swiftlint:disable line_length
                let moyaProvider = MoyaProvider<TechTestEndpoint>(endpointClosure: customEndpointClosure, stubClosure: MoyaProvider.immediatelyStub)
                let resourceProvider = MoyaProvider<ResourceEndpoint>(stubClosure: MoyaProvider.immediatelyStub)

                var assertedNewsItemVMs: [LatestNewsViewModel]?
                let assertedVM = LatestNewsVCViewModel(techProvider: moyaProvider, resProvider: resourceProvider)
                assertedVM.latestNewsObservable.subscribe(onNext: { value in
                    assertedNewsItemVMs = value
                }).disposed(by: disposeBag)

                expect(assertedNewsItemVMs?.count).toEventually(equal(0))
            }
        }
    }
}

import Foundation
import Moya
import RxMoya

public enum ResourceEndpoint {
    case image(url: String)
}

extension ResourceEndpoint: TargetType {
    public var baseURL: URL {
        switch self {
        case .image(let url):
            return URL(string: url) ?? URL(fileURLWithPath: url)
        }
    }

    public var path: String {
        return ""
    }

    public var method: Moya.Method {
        return .get
    }

    public var sampleData: Data {

        let bundle = Bundle(for: LatestNewsItemModel.self)

        switch self {
        case .image:
            if let image = UIImage(named: "sampleImage.jpg", in: bundle, compatibleWith: nil) {
                return image.jpegData(compressionQuality: 0.5) ?? ("Something went wrong").data(using: .utf8)!
            }
            return ("Something went wrong").data(using: .utf8)!
        }
    }

    public var task: Task {
        return .requestPlain
    }

    public var headers: [String: String]? {
        return nil
    }
}

import Moya

public enum TechTestEndpoint {
    case scores
    case latestNews
    case standings
}

extension TechTestEndpoint: TargetType {
    private static let mainUrl = "https://demo6925217.mockable.io/sport"
    public var baseURL: URL { return URL(string: TechTestEndpoint.mainUrl) ?? URL(fileURLWithPath: "")}

    public var path: String {
        switch self {
        case .scores:
            return "scores.xml"
        case .latestNews:
            return "latestnews.xml"
        case .standings:
            return "standings.xml"
        }
    }

    public var method: Moya.Method {
        return .get
    }

    public var sampleData: Data {
        switch self {
        case .scores:
            return sampleEndpointData()
        case .standings:
            return sampleEndpointData()
        case .latestNews:
            return sampleEndpointData()
        }
    }

    public var task: Task {
        return .requestPlain
    }

    public var headers: [String: String]? {
        let headerFields: [String: String] = [
            "Content-Type": "text/xml",
            "Accept": "application/xml"]
        return headerFields
    }
}

// swiftlint:disable all
extension TechTestEndpoint {
    func sampleEndpointData() -> Data {
        var returnDataString : String = ""
        switch self {
        case .scores:
            returnDataString = """
            <gsmrs version=\"2.0\" sport=\"soccer\" lang=\"en\" last_generated=\"2012-09-21 11:52:12\">
            <method method_id=\"11\" name=\"get_matches_live\">
            <parameter name=\"date\" value=\"2012-09-19\"/>
            <parameter name=\"detailed\" value=\"no\"/>
            <parameter name=\"id\" value=\"7238\"/>
            <parameter name=\"lang\" value=\"en\"/>
            <parameter name=\"minutes\" value=\"no\"/>
            <parameter name=\"now_playing\" value=\"no\"/>
            <parameter name=\"type\" value=\"season\"/>
            </method>
            <competition competition_id=\"10\" name=\"UEFA Champions League\" teamtype=\"default\" display_order=\"20\" type=\"club\" area_id=\"7\" area_name=\"Europe\" last_updated=\"2012-09-19 22:59:51\" soccertype=\"default\">
            <season season_id=\"7238\" name=\"2012/2013\" start_date=\"2012-07-03\" end_date=\"2013-05-25\" service_level=\"0\" last_updated=\"2012-09-20 13:26:45\">
            <round round_id=\"17916\" name=\"Group stage\" start_date=\"2012-09-18\" end_date=\"2012-12-05\" type=\"table\" groups=\"8\" has_outgroup_matches=\"no\" last_updated=\"2012-09-20 13:26:45\">
            <group group_id=\"5226\" name=\"Group E\" details=\"\" winner=\"\" last_updated=\"2012-09-20 13:26:45\">
            <match match_id=\"1388658\" date_utc=\"2012-09-19\" time_utc=\"18:45:00\" date_london=\"2012-09-19\" time_london=\"19:45:00\" team_A_id=\"661\" team_A_name=\"Chelsea\" team_A_country=\"ENG\" team_B_id=\"1242\" team_B_name=\"Juventus\" team_B_country=\"ITA\" status=\"Playing\" gameweek=\"1\" winner=\"\" fs_A=\"0\" fs_B=\"2\" hts_A=\"\" hts_B=\"\" ets_A=\"\" ets_B=\"\" ps_A=\"\" ps_B=\"\" last_updated=\"2012-09-20 13:26:45\"/>
            <match match_id=\"1388722\" date_utc=\"2012-09-19\" time_utc=\"18:45:00\" date_london=\"2012-09-19\" time_london=\"19:45:00\" team_A_id=\"2254\" team_A_name=\"Shakhtar Donetsk\" team_A_country=\"UKR\" team_B_id=\"606\" team_B_name=\"Nordsjælland\" team_B_country=\"DNK\" status=\"Playing\" gameweek=\"1\" winner=\"\" fs_A=\"0\" fs_B=\"1\" hts_A=\"\" hts_B=\"\" ets_A=\"\" ets_B=\"\" ps_A=\"\" ps_B=\"\" last_updated=\"2012-09-19 22:52:06\"/>
            </group>
            <group group_id=\"5227\" name=\"Group F\" details=\"\" winner=\"\" last_updated=\"2012-09-20 12:43:11\">
            <match match_id=\"1388723\" date_utc=\"2012-09-19\" time_utc=\"18:45:00\" date_london=\"2012-09-19\" time_london=\"19:45:00\" team_A_id=\"895\" team_A_name=\"Lille\" team_A_country=\"FRA\" team_B_id=\"200\" team_B_name=\"BATE\" team_B_country=\"BLR\" status=\"Playing\" gameweek=\"1\" winner=\"\" fs_A=\"0\" fs_B=\"2\" hts_A=\"\" hts_B=\"\" ets_A=\"\" ets_B=\"\" ps_A=\"\" ps_B=\"\" last_updated=\"2012-09-19 22:56:46\"/>
            <match match_id=\"1388724\" date_utc=\"2012-09-19\" time_utc=\"18:45:00\" date_london=\"2012-09-19\" time_london=\"19:45:00\" team_A_id=\"961\" team_A_name=\"Bayern München\" team_A_country=\"DEU\" team_B_id=\"2015\" team_B_name=\"Valencia\" team_B_country=\"ESP\" status=\"Playing\" gameweek=\"1\" winner=\"\" fs_A=\"0\" fs_B=\"2\" hts_A=\"\" hts_B=\"\" ets_A=\"\" ets_B=\"\" ps_A=\"\" ps_B=\"\" last_updated=\"2012-09-20 12:43:11\"/>
            </group>
            <group group_id=\"5228\" name=\"Group G\" details=\"\" winner=\"\" last_updated=\"2012-09-19 22:51:23\">
            <match match_id=\"1388646\" date_utc=\"2012-09-19\" time_utc=\"18:45:00\" date_london=\"2012-09-19\" time_london=\"19:45:00\" team_A_id=\"2017\" team_A_name=\"Barcelona\" team_A_country=\"ESP\" team_B_id=\"1844\" team_B_name=\"Spartak Moskva\" team_B_country=\"RUS\" status=\"Playing\" gameweek=\"1\" winner=\"\" fs_A=\"0\" fs_B=\"2\" hts_A=\"\" hts_B=\"\" ets_A=\"\" ets_B=\"\" ps_A=\"\" ps_B=\"\" last_updated=\"2012-09-19 22:51:23\"/>
            <match match_id=\"1388725\" date_utc=\"2012-09-19\" time_utc=\"18:45:00\" date_london=\"2012-09-19\" time_london=\"19:45:00\" team_A_id=\"1898\" team_A_name=\"Celtic\" team_A_country=\"SCO\" team_B_id=\"1679\" team_B_name=\"Benfica\" team_B_country=\"PRT\" status=\"Playing\" gameweek=\"1\" winner=\"\" fs_A=\"0\" fs_B=\"2\" hts_A=\"\" hts_B=\"\" ets_A=\"\" ets_B=\"\" ps_A=\"\" ps_B=\"\" last_updated=\"2012-09-19 22:49:52\"/>
            </group>
            <group group_id=\"5229\" name=\"Group H\" details=\"\" winner=\"\" last_updated=\"2012-09-19 22:59:51\">
            <match match_id=\"1388664\" date_utc=\"2012-09-19\" time_utc=\"18:45:00\" date_london=\"2012-09-19\" time_london=\"19:45:00\" team_A_id=\"662\" team_A_name=\"Manchester United\" team_A_country=\"ENG\" team_B_id=\"2217\" team_B_name=\"Galatasaray\" team_B_country=\"TUR\" status=\"Playing\" gameweek=\"1\" winner=\"\" fs_A=\"0\" fs_B=\"2\" hts_A=\"\" hts_B=\"\" ets_A=\"\" ets_B=\"\" ps_A=\"\" ps_B=\"\" last_updated=\"2012-09-19 22:53:37\"/>
            <match match_id=\"1388726\" date_utc=\"2012-09-19\" time_utc=\"18:45:00\" date_london=\"2012-09-19\" time_london=\"19:45:00\" team_A_id=\"1682\" team_A_name=\"Sporting Braga\" team_A_country=\"PRT\" team_B_id=\"1824\" team_B_name=\"CFR Cluj\" team_B_country=\"ROU\" status=\"Playing\" gameweek=\"1\" winner=\"\" fs_A=\"0\" fs_B=\"2\" hts_A=\"\" hts_B=\"\" ets_A=\"\" ets_B=\"\" ps_A=\"\" ps_B=\"\" last_updated=\"2012-09-19 22:59:51\"/>
            </group>
            </round>
            </season>
            </competition>
            </gsmrs>
            """
        case .latestNews:
            returnDataString = """
            <rss version=\"2.0\">
            <channel>
            <title>Perform Mobile Test - Latest News</title>
            <description/>
            <language>en-GB</language>
            <pubDate>Tue, 01 Jan 2013 18:00:00 +0000</pubDate>
            <link>
            https://www.goal.com/en
            </link>
            <category>interview</category>
            <category>Perform</category>
            <item>
            <guid>100001</guid>
            <title>Lead story headline goes here</title>
            <pubDate>Tue, 01 Jan 2013 17:00:00 +0000</pubDate>
            <enclosure length=\"9048\" url=\"http://www.mobilefeeds.performgroup.com/javaImages/4b/ce/0,,14012~11849291,00.jpg\" type=\"image/jpeg\"/>
            <description>Lead story teaser text here</description>
            <link>
            https://www.goal.com/en/news/man-utds-robertson-alexander-arnold-shaw-wan-bissaka-finally/1vbh7n8q5x90l1dkvxi1sq7oob
            </link>
            <category>News</category>
            </item>
            <item>
            <guid>100002</guid>
            <title>2nd story headline goes here</title>
            <pubDate>Tue, 01 Jan 2013 16:00:00 +0000</pubDate>
            <enclosure length=\"9048\" url=\"http://www.mobilefeeds.performgroup.com/javaImages/4b/ce/0,,14012~11849291,00.jpg\" type=\"image/jpeg\"/>
            <description>2nd story teaser text here</description>
            <link>
            https://www.goal.com/en/news/pedro-leon-how-real-madrids-potential-star-became-mourinhos/1r5mn1imd1hko1gh912x9m8bs7
            </link>
            <category>News</category>
            </item>
            <item>
            <guid>100003</guid>
            <title>3rd story headline goes here</title>
            <pubDate>Tue, 01 Jan 2013 15:00:00 +0000</pubDate>
            <enclosure length=\"9048\" url=\"http://www.mobilefeeds.performgroup.com/javaImages/4b/ce/0,,14012~11849291,00.jpg\" type=\"image/jpeg\"/>
            <description>3rd story teaser text here</description>
            <link>
            https://www.goal.com/en/news/dominant-diallo-shines-for-man-utd-u23s-once-again-as/1490vkb7bexbn1808lq4fpyhn5
            </link>
            <category>News</category>
            </item>
            <item>
            <guid>100004</guid>
            <title>4th story headline goes here</title>
            <pubDate>Tue, 01 Jan 2013 14:00:00 +0000</pubDate>
            <enclosure length=\"9048\" url=\"http://www.mobilefeeds.performgroup.com/javaImages/4b/ce/0,,14012~11849291,00.jpg\" type=\"image/jpeg\"/>
            <description>4th story teaser text here</description>
            <link>
            https://www.goal.com/en/news/juventus-star-ronaldo-on-birthday-im-sorry-i-cant-promise-20/1xvh8n4smdbya17qf4n39o6z4t
            </link>
            <category>News</category>
            </item>
            <item>
            <guid>100005</guid>
            <title>5th story headline goes here</title>
            <pubDate>Tue, 01 Jan 2013 13:00:00 +0000</pubDate>
            <enclosure length=\"9048\" url=\"http://www.mobilefeeds.performgroup.com/javaImages/4b/ce/0,,14012~11849291,00.jpg\" type=\"image/jpeg\"/>
            <description>5th story teaser text here</description>
            <link>
            https://www.goal.com/en/news/gundogan-one-of-klopps-favourites-who-can-settle-title-race/nodr17wd0rv1omk0s0sz6tgs
            </link>
            <category>News</category>
            </item>
            <item>
            <guid>100006</guid>
            <title>6th story headline goes here</title>
            <pubDate>Tue, 01 Jan 2013 12:00:00 +0000</pubDate>
            <enclosure length=\"9048\" url=\"http://www.mobilefeeds.performgroup.com/javaImages/4b/ce/0,,14012~11849291,00.jpg\" type=\"image/jpeg\"/>
            <description>6th story teaser text here</description>
            <link>
            https://www.goal.com/en/news/aubameyang-or-pepe-artetas-big-decision-as-arsenals-72m-star/16xdmkzf235nd1ty6317sabceo
            </link>
            <category>News</category>
            </item>
            <item>
            <guid>100007</guid>
            <title>7th story headline goes here</title>
            <pubDate>Tue, 01 Jan 2013 11:00:00 +0000</pubDate>
            <enclosure length=\"9048\" url=\"http://www.mobilefeeds.performgroup.com/javaImages/4b/ce/0,,14012~11849291,00.jpg\" type=\"image/jpeg\"/>
            <description>7th story teaser text here</description>
            <link>
            https://www.goal.com/en/news/guardiola-hits-back-at-klopp-two-week-break-claim/s6upfx0gg0r110ndvt9vyxqnn
            </link>
            <category>News</category>
            </item>
            <item>
            <guid>100008</guid>
            <title>8th story headline goes here</title>
            <pubDate>Tue, 01 Jan 2013 10:00:00 +0000</pubDate>
            <enclosure length=\"9048\" url=\"http://www.mobilefeeds.performgroup.com/javaImages/4b/ce/0,,14012~11849291,00.jpg\" type=\"image/jpeg\"/>
            <description>8th story teaser text here</description>
            <link>
            http://www.mobilefeeds.performgroup.com/utilities/interviews/techtest/webview/article.html/guid/100008
            </link>
            <category>News</category>
            </item>
            <item>
            <guid>100009</guid>
            <title>9th story headline goes here</title>
            <pubDate>Tue, 01 Jan 2013 09:00:00 +0000</pubDate>
            <enclosure length=\"9048\" url=\"http://www.mobilefeeds.performgroup.com/javaImages/4b/ce/0,,14012~11849291,00.jpg\" type=\"image/jpeg\"/>
            <description>9th story teaser text here</description>
            <link>
            http://www.mobilefeeds.performgroup.com/utilities/interviews/techtest/webview/article.html/guid/100009
            </link>
            <category>News</category>
            </item>
            <item>
            <guid>100010</guid>
            <title>10th story headline goes here</title>
            <pubDate>Tue, 01 Jan 2013 08:00:00 +0000</pubDate>
            <enclosure length=\"9048\" url=\"http://www.mobilefeeds.performgroup.com/javaImages/4b/ce/0,,14012~11849291,00.jpg\" type=\"image/jpeg\"/>
            <description>10th story teaser text here</description>
            <link>
            http://www.mobilefeeds.performgroup.com/utilities/interviews/techtest/webview/article.html/guid/100010
            </link>
            <category>News</category>
            </item>
            </channel>
            </rss>
            """
            
        case .standings:
            returnDataString = """
            <gsmrs version=\"2.0\" sport=\"soccer\" lang=\"en\" last_generated=\"2013-04-17 15:26:15\">
            <method method_id=\"12\" name=\"get_tables\">
            <parameter name=\"id\" value=\"5983\"/>
            <parameter name=\"lang\" value=\"en\"/>
            <parameter name=\"tabletype\" value=\"total\"/>
            <parameter name=\"type\" value=\"season\"/>
            </method>
            <competition competition_id=\"8\" name=\"Premier League\" teamtype=\"default\" display_order=\"10\" type=\"club\" area_id=\"68\" area_name=\"England\" last_updated=\"2013-04-17 15:21:27\" soccertype=\"default\">
            <season season_id=\"5983\" name=\"2011/2012\" start_date=\"2011-08-13\" end_date=\"2012-05-30\" service_level=\"0\" last_updated=\"2012-07-12 10:07:57\" last_playedmatch_date=\"2012-05-13\">
            <round round_id=\"14829\" name=\"Regular Season\" start_date=\"2011-08-13\" end_date=\"2012-05-30\" type=\"table\" ordermethod=\"1\" groups=\"0\" has_outgroup_matches=\"no\">
            <resultstable type=\"total\">
            <ranking rank=\"1\" last_rank=\"1\" zone_start=\"CL\" team_id=\"676\" club_name=\"Manchester City\" countrycode=\"ENG\" area_id=\"68\" matches_total=\"38\" matches_won=\"28\" matches_draw=\"5\" matches_lost=\"5\" goals_pro=\"93\" goals_against=\"29\" points=\"89\"/>
            <ranking rank=\"2\" last_rank=\"2\" team_id=\"662\" club_name=\"Manchester United\" countrycode=\"ENG\" area_id=\"68\" matches_total=\"38\" matches_won=\"28\" matches_draw=\"5\" matches_lost=\"5\" goals_pro=\"89\" goals_against=\"33\" points=\"89\"/>
            <ranking rank=\"3\" last_rank=\"3\" zone_end=\"CL\" team_id=\"660\" club_name=\"Arsenal\" countrycode=\"ENG\" area_id=\"68\" matches_total=\"38\" matches_won=\"21\" matches_draw=\"7\" matches_lost=\"10\" goals_pro=\"74\" goals_against=\"49\" points=\"70\"/>
            <ranking rank=\"4\" last_rank=\"4\" zone_start=\"CL qf\" zone_end=\"CL qf\" team_id=\"675\" club_name=\"Tottenham Hotspur\" countrycode=\"ENG\" area_id=\"68\" matches_total=\"38\" matches_won=\"20\" matches_draw=\"9\" matches_lost=\"9\" goals_pro=\"66\" goals_against=\"41\" points=\"69\"/>
            <ranking rank=\"5\" last_rank=\"5\" zone_start=\"EuroL\" zone_end=\"EuroL\" team_id=\"664\" club_name=\"Newcastle United\" countrycode=\"ENG\" area_id=\"68\" matches_total=\"38\" matches_won=\"19\" matches_draw=\"8\" matches_lost=\"11\" goals_pro=\"56\" goals_against=\"51\" points=\"65\"/>
            <ranking rank=\"6\" last_rank=\"6\" team_id=\"661\" club_name=\"Chelsea\" countrycode=\"ENG\" area_id=\"68\" matches_total=\"38\" matches_won=\"18\" matches_draw=\"10\" matches_lost=\"10\" goals_pro=\"65\" goals_against=\"46\" points=\"64\"/>
            <ranking rank=\"7\" last_rank=\"7\" team_id=\"674\" club_name=\"Everton\" countrycode=\"ENG\" area_id=\"68\" matches_total=\"38\" matches_won=\"15\" matches_draw=\"11\" matches_lost=\"12\" goals_pro=\"50\" goals_against=\"40\" points=\"56\"/>
            <ranking rank=\"8\" last_rank=\"8\" team_id=\"663\" club_name=\"Liverpool\" countrycode=\"ENG\" area_id=\"68\" matches_total=\"38\" matches_won=\"14\" matches_draw=\"10\" matches_lost=\"14\" goals_pro=\"47\" goals_against=\"40\" points=\"52\"/>
            <ranking rank=\"9\" last_rank=\"9\" team_id=\"667\" club_name=\"Fulham\" countrycode=\"ENG\" area_id=\"68\" matches_total=\"38\" matches_won=\"14\" matches_draw=\"10\" matches_lost=\"14\" goals_pro=\"48\" goals_against=\"51\" points=\"52\"/>
            <ranking rank=\"10\" last_rank=\"10\" team_id=\"678\" club_name=\"West Bromwich Albion\" countrycode=\"ENG\" area_id=\"68\" matches_total=\"38\" matches_won=\"13\" matches_draw=\"8\" matches_lost=\"17\" goals_pro=\"45\" goals_against=\"52\" points=\"47\"/>
            <ranking rank=\"11\" last_rank=\"12\" team_id=\"738\" club_name=\"Swansea City\" countrycode=\"WAL\" area_id=\"209\" matches_total=\"38\" matches_won=\"12\" matches_draw=\"11\" matches_lost=\"15\" goals_pro=\"44\" goals_against=\"51\" points=\"47\"/>
            <ranking rank=\"12\" last_rank=\"14\" team_id=\"677\" club_name=\"Norwich City\" countrycode=\"ENG\" area_id=\"68\" matches_total=\"38\" matches_won=\"12\" matches_draw=\"11\" matches_lost=\"15\" goals_pro=\"52\" goals_against=\"66\" points=\"47\"/>
            <ranking rank=\"13\" last_rank=\"11\" team_id=\"683\" club_name=\"Sunderland\" countrycode=\"ENG\" area_id=\"68\" matches_total=\"38\" matches_won=\"11\" matches_draw=\"12\" matches_lost=\"15\" goals_pro=\"45\" goals_against=\"46\" points=\"45\"/>
            <ranking rank=\"14\" last_rank=\"13\" team_id=\"690\" club_name=\"Stoke City\" countrycode=\"ENG\" area_id=\"68\" matches_total=\"38\" matches_won=\"11\" matches_draw=\"12\" matches_lost=\"15\" goals_pro=\"36\" goals_against=\"53\" points=\"45\"/>
            <ranking rank=\"15\" last_rank=\"16\" team_id=\"686\" club_name=\"Wigan Athletic\" countrycode=\"ENG\" area_id=\"68\" matches_total=\"38\" matches_won=\"11\" matches_draw=\"10\" matches_lost=\"17\" goals_pro=\"42\" goals_against=\"62\" points=\"43\"/>
            <ranking rank=\"16\" last_rank=\"15\" team_id=\"665\" club_name=\"Aston Villa\" countrycode=\"ENG\" area_id=\"68\" matches_total=\"38\" matches_won=\"7\" matches_draw=\"17\" matches_lost=\"14\" goals_pro=\"37\" goals_against=\"53\" points=\"38\"/>
            <ranking rank=\"17\" last_rank=\"17\" team_id=\"702\" club_name=\"Queens Park Rangers\" countrycode=\"ENG\" area_id=\"68\" matches_total=\"38\" matches_won=\"10\" matches_draw=\"7\" matches_lost=\"21\" goals_pro=\"43\" goals_against=\"66\" points=\"37\"/>
            <ranking rank=\"18\" last_rank=\"18\" zone_start=\"releg\" team_id=\"666\" club_name=\"Bolton Wanderers\" countrycode=\"ENG\" area_id=\"68\" matches_total=\"38\" matches_won=\"10\" matches_draw=\"6\" matches_lost=\"22\" goals_pro=\"46\" goals_against=\"77\" points=\"36\"/>
            <ranking rank=\"19\" last_rank=\"19\" team_id=\"672\" club_name=\"Blackburn Rovers\" countrycode=\"ENG\" area_id=\"68\" matches_total=\"38\" matches_won=\"8\" matches_draw=\"7\" matches_lost=\"23\" goals_pro=\"48\" goals_against=\"78\" points=\"31\"/>
            <ranking rank=\"20\" last_rank=\"20\" zone_end=\"releg\" team_id=\"680\" club_name=\"Wolverhampton Wanderers\" countrycode=\"ENG\" area_id=\"68\" matches_total=\"38\" matches_won=\"5\" matches_draw=\"10\" matches_lost=\"23\" goals_pro=\"40\" goals_against=\"82\" points=\"25\"/>
            </resultstable>
            </round>
            </season>
            </competition>
            </gsmrs>
            """
        }
        
        return returnDataString.data(using: .utf8) ?? Data()
    }
}
// swiftlint:enable all

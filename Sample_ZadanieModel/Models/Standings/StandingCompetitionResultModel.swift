import XMLMapper

public class StandingCompetitionResultModel: XMLMappable {

    public var rank: Int?
    public var lastRank: Int?
    public var clubName: String?
    public var matchesTotal: Int?
    public var matchesWon: Int?
    public var matchesDraw: Int?
    public var matchesLost: Int?
    public var goalsPro: Int?
    public var goalsAgainst: Int?
    public var points: Int?

    public required init?(map: XMLMap) {

    }

    public var nodeName: String! = ""

    public func mapping(map: XMLMap) {
        rank <- map.attributes["rank"]
        lastRank <- map.attributes["last_rank"]
        clubName <- map.attributes["club_name"]
        matchesTotal <- map.attributes["matches_total"]
        matchesWon <- map.attributes["matches_won"]
        matchesDraw <- map.attributes["matches_draw"]
        matchesLost <- map.attributes["matches_lost"]
        goalsPro <- map.attributes["goals_pro"]
        goalsAgainst <- map.attributes["goals_against"]
        points <- map.attributes["points"]
    }
}

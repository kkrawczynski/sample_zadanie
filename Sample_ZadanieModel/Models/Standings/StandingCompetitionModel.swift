import XMLMapper

public class StandingCompetitionModel: XMLMappable {
    public var competitionResults: [StandingCompetitionResultModel]?
    public var competitionName: String?
    public required init?(map: XMLMap) {
        guard let seasonNode = (map.XML["season"]) as? [String: AnyObject] else {
            return nil
        }
        guard let roundNode = seasonNode["round"] as? [String: AnyObject] else {
            return nil
        }
        if roundNode["resultstable"] == nil {
            return nil
        }
    }

    public var nodeName: String!

    public func mapping(map: XMLMap) {
        competitionName <- map.attributes["name"]
        competitionResults <- map["season.round.resultstable.ranking"]
    }
}

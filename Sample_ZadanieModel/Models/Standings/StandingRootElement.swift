import XMLMapper

public class StandingRootElement: XMLMappable {

    public var competition: StandingCompetitionModel?

    public required init?(map: XMLMap) {
        if map.XML["competition"] == nil {
            return nil
        }
    }

    public var nodeName: String!

    public func mapping(map: XMLMap) {
        competition <- map["competition"]
    }
}

import XMLMapper

public class ScoresMethodInfoModel: XMLMappable {
    public var parameters: [ScoresMethodParameterModel]?

    public required init?(map: XMLMap) {
        if map.XML["parameter"] == nil {
            return nil
        }
    }

    public var nodeName: String!

    public func mapping(map: XMLMap) {
        parameters <- map["parameter"]
    }
}

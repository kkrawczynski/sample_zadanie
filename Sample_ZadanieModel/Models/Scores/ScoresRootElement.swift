import XMLMapper

public class ScoresRootElement: XMLMappable {
    public var methodInfo: ScoresMethodInfoModel?
    public var competition: ScoresCompetitionModel?

    public required init?(map: XMLMap) {
        if map.XML["method"] == nil {
            return nil
        }

        if map.XML["competition"] == nil {
            return nil
        }
    }

    public var nodeName: String!

    public func mapping(map: XMLMap) {
        methodInfo <- map["method"]
        competition <- map["competition"]
    }
}

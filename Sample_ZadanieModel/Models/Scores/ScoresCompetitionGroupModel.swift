import XMLMapper

public class ScoresCompetitionGroupModel: XMLMappable {
    public var competitionResults: [ScoresCompetitionResultModel]?
    public var groupName: String?

    public required init?(map: XMLMap) {
        if map.XML["match"] == nil {
            return nil
        }
    }

    public var nodeName: String!

    public func mapping(map: XMLMap) {
        groupName <- map.attributes["name"]
        competitionResults <- map["match"]
    }
}

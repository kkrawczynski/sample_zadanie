import XMLMapper

public class ScoresMethodParameterModel: XMLMappable {
    public var name: String?
    public var value: String?

    public required init?(map: XMLMap) {

    }

    public var nodeName: String!

    public func mapping(map: XMLMap) {
        name <- map.attributes["name"]
        value <- map.attributes["value"]
    }
}

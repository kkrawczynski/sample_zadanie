import XMLMapper

public class ScoresCompetitionModel: XMLMappable {
    public var name: String?
    public var competitionGroups: [ScoresCompetitionGroupModel]?

    public required init?(map: XMLMap) {
        guard let seasonNode = (map.XML["season"]) as? [String: AnyObject] else {
            return nil
        }
        if seasonNode["round"] == nil {
            return nil
        }
    }

    public var nodeName: String!

    public func mapping(map: XMLMap) {
       name <- map.attributes["name"]
        competitionGroups <- map["season.round.group"]
    }
}

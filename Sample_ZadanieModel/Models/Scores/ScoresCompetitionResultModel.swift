import XMLMapper

public class ScoresCompetitionResultModel: XMLMappable {

    public var teamAName: String?
    public var teamACountry: String?
    public var teamBName: String?
    public var teamBCountry: String?
    public var ftScoreA: Int?
    public var ftScoreB: Int?

    public required init?(map: XMLMap) {

    }

    public var nodeName: String!

    public func mapping(map: XMLMap) {
        teamAName <- map.attributes["team_A_name"]
        teamACountry <- map.attributes["team_A_country"]
        teamBName <- map.attributes["team_B_name"]
        teamBCountry <- map.attributes["team_B_country"]
        ftScoreA <- map.attributes["fs_A"]
        ftScoreB <- map.attributes["fs_B"]

    }
}

import XMLMapper

public class LatestNewsChannelModel: XMLMappable {
    // MARK: Vars
    public var title: String?
    public var description: String?
    public var language: String?
    public var pubDate: Date?
    public var link: String?
    public var categories: [String]?
    public var items: [LatestNewsItemModel]?

    // MARK: XMLMappable
    public required init?(map: XMLMap) {

    }

    public var nodeName: String! = "channel"

    public func mapping(map: XMLMap) {
        title <- map["title"]
        description <- map["description"]
        language <- map["language"]

        pubDate <- (map["pubDate"], LatestNewsDateTransformer())
        link <- map["link"]
        categories <- map["category"]
        items <- map["item"]

    }
}

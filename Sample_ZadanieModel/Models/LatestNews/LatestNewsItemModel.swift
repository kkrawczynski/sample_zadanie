import XMLMapper

public class LatestNewsItemModel: XMLMappable {
    // MARK: Vars
    public var title: String?
    public var pubDate: Date?
    public var thumbnailUrl: String?
    public var description: String?
    public var link: String?
    public var category: String?

    // MARK: XMLMappable
    public required init?(map: XMLMap) {

    }

    public var nodeName: String! = "item"

    public func mapping(map: XMLMap) {
        title <- map["title"]
        pubDate <- (map["pubDate"], LatestNewsDateTransformer())
        description <- map["description"]
        link <- map["link"]
        category <- map["category"]
        thumbnailUrl <- map.attributes["enclosure.url"]
    }
}

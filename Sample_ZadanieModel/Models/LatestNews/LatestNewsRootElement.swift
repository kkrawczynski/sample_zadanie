import XMLMapper

public class LatestNewsRootElement: XMLMappable {

    public var channelElements: [LatestNewsChannelModel]?

    public required init?(map: XMLMap) {
        if map.XML["channel"] == nil {
            return nil
        }
    }

    public var nodeName: String!

    public func mapping(map: XMLMap) {
        channelElements <- map["channel"]
    }

}

import XMLMapper

class LatestNewsDateTransformer: XMLTransformType {
    func transformFromXML(_ value: Any?) -> Date? {
        if let dateValue = value as? String {
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "E, dd MMM yyyy HH:mm:ss Z"
            return dateFormatter.date(from: dateValue)
        }
        return nil
    }

    func transformToXML(_ value: Date?) -> String? {
        return "\(String(describing: value))"
    }

    typealias Object = Date
    typealias XML = String
}

class ScoresDateTransformer: XMLTransformType {
    func transformFromXML(_ value: Any?) -> Date? {
        if let dateValue = value as? String {
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "yyyy-MM-dd"
            return dateFormatter.date(from: dateValue)
        }
        return nil
    }

    func transformToXML(_ value: Date?) -> String? {
        return "\(String(describing: value))"
    }

    typealias Object = Date
    typealias XML = String
}
